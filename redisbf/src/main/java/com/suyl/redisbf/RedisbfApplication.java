package com.suyl.redisbf;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

@SpringBootApplication
public class RedisbfApplication {

    public static void main(String[] args) {
        SpringApplication.run(RedisbfApplication.class, args);
        // 查询ip地址
        try {
            Document document = Jsoup.connect("http://chaipip.com/").get();
            Elements elements = document.select("#ip");
            System.out.println(elements.attr("value"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
