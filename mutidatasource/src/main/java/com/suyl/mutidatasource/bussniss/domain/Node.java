package com.suyl.mutidatasource.bussniss.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 流程节点名称表
 * </p>
 *
 * @author suyanlong
 * @since 2020-07-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("process_node")
public class Node implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 流程节点名称id
     */
    @TableId(value = "taskNameId", type = IdType.AUTO)
    private Integer taskNameId;

    /**
     * 流程定义key
     */
    @TableField("processDefinitionKey")
    private String processDefinitionKey;

    /**
     * 流程节点中文名称
     */
    @TableField("taskName")
    private String taskName;

    /**
     * 创建时间
     */
    @TableField("createTime")
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @TableField("updateTime")
    private LocalDateTime updateTime;

    /**
     * 前节点id（taskNameId）
     */
    @TableField("frontNodeId")
    private Integer frontNodeId;

    /**
     * 跳转地址
     */
    @TableField("url")
    private String url;


}
