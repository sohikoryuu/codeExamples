package com.suyl.mutidatasource.bussniss.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.suyl.mutidatasource.bussniss.domain.Node;


/**
 * <p>
 * 流程节点名称表 服务类
 * </p>
 *
 * @author suyanlong
 * @since 2020-07-31
 */
public interface NodeService extends IService<Node> {

    boolean save1(Node node);

    boolean save2(Node node);
}
