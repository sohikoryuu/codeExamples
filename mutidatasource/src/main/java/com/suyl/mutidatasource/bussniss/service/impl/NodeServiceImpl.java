package com.suyl.mutidatasource.bussniss.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.suyl.mutidatasource.bussniss.dao.NodeDao;
import com.suyl.mutidatasource.bussniss.domain.Node;
import com.suyl.mutidatasource.bussniss.service.NodeService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 流程节点名称表 服务实现类
 * </p>
 *
 * @author suyanlong
 * @since 2020-07-31
 */
@Service
public class NodeServiceImpl extends ServiceImpl<NodeDao, Node> implements NodeService {

    @Override
    public boolean save1(Node node) {
        baseMapper.insert(node);
        return false;
    }

    @Override
    @DS("slave_1")
    public boolean save2(Node node) {
        baseMapper.insert(node);
        return false;
    }
}
