package com.suyl.mutidatasource.bussniss.controller;


import com.suyl.mutidatasource.bussniss.domain.Node;
import com.suyl.mutidatasource.bussniss.service.NodeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

/**
 * <p>
 * 流程节点名称表 前端控制器
 * </p>
 *
 * @author suyanlong
 * @since 2020-07-31
 */
@Slf4j
@RestController
@RequestMapping("/node")
public class NodeController {

    @Autowired
    NodeService nodeService;

    /**
     * 流程节点名称 保存 完善信息
     *
     * @return
     */
    @GetMapping(value = "/save1")
    public void save() {
        log.info("流程节点名称 保存 完善信息");
        try {
            Node node = new Node();
            node.setProcessDefinitionKey("save1");
            node.setCreateTime(LocalDateTime.now());
            node.setTaskName("save1");
            nodeService.save1(node);
        } catch (Exception e) {
            log.error("流程节点名称 保存 完善信息发生异常", e);
        }
    }

    /**
     * 流程节点名称 保存 完善信息
     *
     * @return
     */
    @GetMapping(value = "/save2")
    public void save2() {
        log.info("流程节点名称 保存 完善信息");
        try {
            Node node = new Node();
            node.setProcessDefinitionKey("save1");
            node.setCreateTime(LocalDateTime.now());
            node.setTaskName("save1");
            nodeService.save2(node);

        } catch (Exception e) {
            log.error("流程节点名称 保存 完善信息发生异常", e);
        }
    }
}

