package com.suyl.mutidatasource.bussniss.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.suyl.mutidatasource.bussniss.domain.Node;

/**
 * <p>
 * 流程节点名称表 Mapper 接口
 * </p>
 *
 * @author suyanlong
 * @since 2020-07-31
 */
public interface NodeDao extends BaseMapper<Node> {

}
