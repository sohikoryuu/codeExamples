package com.suyl.mutidatasource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MutidatasourceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MutidatasourceApplication.class, args);
    }

}
