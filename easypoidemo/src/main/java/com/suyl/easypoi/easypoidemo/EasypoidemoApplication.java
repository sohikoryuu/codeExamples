package com.suyl.easypoi.easypoidemo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EasypoidemoApplication {

    private static final Logger log = LoggerFactory.getLogger(EasypoidemoApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(EasypoidemoApplication.class, args);
        log.info("EasypoidemoApplication is success");
    }
}
