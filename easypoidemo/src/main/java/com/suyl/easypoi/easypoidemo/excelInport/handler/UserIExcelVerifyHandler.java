package com.suyl.easypoi.easypoidemo.excelInport.handler;

import cn.afterturn.easypoi.excel.entity.result.ExcelVerifyHandlerResult;
import cn.afterturn.easypoi.handler.inter.IExcelVerifyHandler;
import com.suyl.easypoi.easypoidemo.excelInport.entity.User;

import java.util.ArrayList;
import java.util.List;

/**
 * @version v1.0
 * @author: suyl
 * @date: 2020/3/19 16:03
 * @Description: No Description
 */
public class UserIExcelVerifyHandler implements IExcelVerifyHandler<User> {
    @Override
    public ExcelVerifyHandlerResult verifyHandler(User o) {
        //减免原因验证
        List<String> list = new ArrayList<>(5);
        list.add("苏彦龙");
        list.add("suyl");
        list.add("candy");
        list.add("suyanlong");
        list.add("苏111");
        if (!list.contains(o.getName())) {
            return new ExcelVerifyHandlerResult(false, "姓名不存在！！！！");
        }
        return new ExcelVerifyHandlerResult(true);
    }
}
