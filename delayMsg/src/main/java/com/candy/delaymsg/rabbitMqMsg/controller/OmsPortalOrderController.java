package com.candy.delaymsg.rabbitMqMsg.controller;

import com.candy.delaymsg.rabbitMqMsg.service.OmsPortalOrderService;
import com.candy.delaymsg.rabbitMqMsg.entity.OrderParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author suYanLong
 * @date 2022年06月23日 10:14
 */
@RestController
@RequestMapping("/order")
public class OmsPortalOrderController {

    @Autowired
    private OmsPortalOrderService portalOrderService;

    @GetMapping(value = "/generateOrder")
    public Object generateOrder(OrderParam orderParam) {
        return portalOrderService.generateOrder(orderParam);
    }

}
