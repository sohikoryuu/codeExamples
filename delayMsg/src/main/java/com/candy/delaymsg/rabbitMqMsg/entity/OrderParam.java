package com.candy.delaymsg.rabbitMqMsg.entity;

import lombok.Data;

/**
 * @author suYanLong
 * @date 2022年06月23日 10:17
 */
@Data
public class OrderParam {

    private Long orderId;

    private Long times;

}
