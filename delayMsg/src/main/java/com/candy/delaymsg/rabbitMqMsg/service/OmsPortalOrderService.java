package com.candy.delaymsg.rabbitMqMsg.service;

import com.candy.delaymsg.rabbitMqMsg.entity.OrderParam;

public interface OmsPortalOrderService {

    void cancelOrder(Long orderId);

    Object generateOrder(OrderParam orderParam);

}
