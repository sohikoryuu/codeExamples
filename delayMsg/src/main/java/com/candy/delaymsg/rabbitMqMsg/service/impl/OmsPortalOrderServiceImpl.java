package com.candy.delaymsg.rabbitMqMsg.service.impl;

import com.candy.delaymsg.rabbitMqMsg.entity.CommonResult;
import com.candy.delaymsg.rabbitMqMsg.entity.OrderParam;
import com.candy.delaymsg.rabbitMqMsg.sender.CancelOrderSender;
import com.candy.delaymsg.rabbitMqMsg.service.OmsPortalOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author suYanLong
 * @date 2022年06月23日 10:12
 */
@Slf4j
@Service
public class OmsPortalOrderServiceImpl implements OmsPortalOrderService {

    @Autowired
    private CancelOrderSender cancelOrderSender;

    @Override
    public CommonResult generateOrder(OrderParam orderParam) {
        //todo 执行一系类下单操作，具体参考mall项目
        log.info("process generateOrder");
        //下单完成后开启一个延迟消息，用于当用户没有付款时取消订单（orderId应该在下单后生成）
        sendDelayMessageCancelOrder(orderParam.getOrderId(), orderParam.getTimes());
        return CommonResult.success(null, "下单成功");
    }

    @Override
    public void cancelOrder(Long orderId) {
        //todo 执行一系类取消订单操作，具体参考mall项目
        log.info("process cancelOrder orderId:{}", orderId);
    }

    private void sendDelayMessageCancelOrder(Long orderId, Long times) {
        //获取订单超时时间，假设为60分钟
        long delayTimes = times * 1000;
        //发送延迟消息
        cancelOrderSender.sendMessage(orderId, delayTimes);
    }

}
