package com.candy.delaymsg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DelayMsgApplication {

    // rabbitMq延时消息
    // https://blog.csdn.net/weixin_45879810/article/details/117075681
    // http://localhost:8080/order/generateOrder?orderId=33&times=10
    public static void main(String[] args) {
        SpringApplication.run(DelayMsgApplication.class, args);
    }

}
