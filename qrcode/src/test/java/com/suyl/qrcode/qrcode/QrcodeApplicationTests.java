package com.suyl.qrcode.qrcode;

import org.iherus.codegen.Codectx;
import org.iherus.codegen.qrcode.QrcodeConfig;
import org.iherus.codegen.qrcode.QreyesFormat;
import org.iherus.codegen.qrcode.SimpleQrcodeGenerator;
import org.iherus.codegen.utils.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

@RunWith(SpringRunner.class)
@SpringBootTest
public class QrcodeApplicationTests {

    /**
     * 二维码生成，参考：https://gitee.com/iherus/qrext4j
     *
     * @throws IOException
     */
    @Test
    public void contextLoads() throws IOException {
        String content = "https://baike.baidu.com/item/%E5%97%B7%E5%A4%A7%E5%96%B5/19817560?fr=aladdin";

        new SimpleQrcodeGenerator().generate(content).toFile("F:\\AodaCat_default_demo1.png");

        OutputStream out = null;
        try {
            out = new FileOutputStream("F:\\AodaCat_default_demo2.png");
            new SimpleQrcodeGenerator().generate(content).toStream(out);

        } finally {
            IOUtils.closeQuietly(out);
        }
        // 设置本地logo
        new SimpleQrcodeGenerator().setLogo("F:\\AodaCat_default_demo1.png").generate(content).toFile("F:\\AodaCat_local_logo.png");

        // 设置在线logo
        String content1 = "https://www.apple.com/cn/";

        String logoUrl = "https://www.baidu.com/img/bd_logo1.png";

        new SimpleQrcodeGenerator().setRemoteLogo(logoUrl).generate(content1).toFile("F:\\Apple_remote_logo.png");

        // 自定义配置
        QrcodeConfig config = new QrcodeConfig()
                .setBorderSize(2)
                .setPadding(10)
                .setMasterColor("#00BFFF")
                .setLogoBorderColor("#B0C4DE");

        String content2 = "https://baike.baidu.com/item/%E5%97%B7%E5%A4%A7%E5%96%B5/19817560?fr=aladdin";

        new SimpleQrcodeGenerator(config).setLogo("F:\\AodaCat_default_demo1.png").generate(content2).toFile("F:\\AodaCat_custom.png");
        // 自定义码眼样式（v1.3.0新增）
        QrcodeConfig config1 = new QrcodeConfig()
                .setBorderSize(2)
                .setPadding(10)
                .setMasterColor("#778899")
                .setLogoBorderColor("#B0C4DE")
                .setCodeEyesPointColor("#BC8F8F")
                .setCodeEyesFormat(QreyesFormat.DR2_BORDER_C_POINT);

        String content5 = "https://baike.baidu.com/item/%E5%97%B7%E5%A4%A7%E5%96%B5/19817560?fr=aladdin";

        new SimpleQrcodeGenerator(config1).setLogo("F:\\AodaCat_default_demo1.png").generate(content5).toFile("F:\\AodaCat_custom_1.png");

        // 圆形logo（v1.3.1新增）
        QrcodeConfig config6 = new QrcodeConfig()
                .setMasterColor("#5F9EA0")
                .setLogoBorderColor("#FFA07A")
                .setLogoShape(Codectx.LogoShape.CIRCLE);
        String content6 = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        new SimpleQrcodeGenerator(config6).setLogo("F:\\AodaCat_default_demo1.png").generate(content6).toFile("F:\\qrcode-circle.png");
    }

}
