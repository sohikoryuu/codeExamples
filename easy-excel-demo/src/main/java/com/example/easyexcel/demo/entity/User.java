package com.example.easyexcel.demo.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.example.easyexcel.demo.converter.GenderConverter;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;

/**
 * 用户人员实体
 *
 * @author fraser
 * @date 2019/10/6 10:05 AM
 */
@Data
public class User {

    /**
     * 姓名
     */
    @ExcelProperty(value = "姓名")
    @Length(min = 1, max = 2)
    private String name;

    /**
     * 年龄
     */
    @ExcelProperty(value = "年龄")
    @Max(27)
    private Integer age;

    /**
     * 性别 1：男；2：女
     */
    @ExcelProperty(value = "性别", converter = GenderConverter.class)
    private Integer gender;

    /**
     * 出生日期
     */
    @ExcelProperty(value = "出生日期")
    @DateTimeFormat("yyyy-MM-dd HH:mm:ss")
    private String birth;

    /**
     * 是否已婚
     */
//	@ExcelProperty(index = 4)
//	@ExcelIgnore
//	private boolean married;
}
