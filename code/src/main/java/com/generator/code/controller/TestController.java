package com.generator.code.controller;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author suyanlong
 * @Description TODO
 * @Date 2021/5/18 20:52
 * @Version 1.0
 */
@RestController
public class TestController implements ApplicationContextAware {

    private ApplicationContext context;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }


    @GetMapping(value = "/test")
    public String test() {
        System.out.println("test --- start");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("test --- end");
        return "test";
    }

    /**
     * 停机
     */
    @GetMapping(value = "shutdown")
    public void shutdown() {
        ConfigurableApplicationContext cyx = (ConfigurableApplicationContext) context;
        cyx.close();
    }
}