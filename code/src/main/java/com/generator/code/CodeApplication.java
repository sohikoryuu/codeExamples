package com.generator.code;

import com.generator.code.config.ElegantShutdownConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;

/**
 * 关闭如何优雅的springBoot项目
 * https://mp.weixin.qq.com/s/zul3S87wgU3mMN_tYiR4Dw
 * <p>
 * 方式一：kill -9 pid
 * 方式二：kill -15 pid
 * 方式三：http://localhost:8998/shutdown
 * 方式四：http://localhost:8998/actuator/shutdown 【推荐】
 */
@SpringBootApplication
public class CodeApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(CodeApplication.class, args);
        run.registerShutdownHook();
    }

    @Bean
    public ElegantShutdownConfig elegantShutdownConfig() {
        return new ElegantShutdownConfig();
    }

    @Bean
    public ServletWebServerFactory servletContainer() {
        TomcatServletWebServerFactory tomcat = new TomcatServletWebServerFactory();
        tomcat.addConnectorCustomizers(elegantShutdownConfig());
        return tomcat;
    }
}
