package com.generator.code.test;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

/**
 * @Author suyanlong
 * @Description TODO
 * @Date 2021/5/13 20:54
 * @Version 1.0
 */
public class MyBatisGenerator {

    public static String author = "suyanlong";
    public static String ourDir = "D:\\suyl\\codeGen";

    public static void main(String[] args) {
        String packageName = "com.cdeledu.user";
        boolean serviceNameStartWithI = false;//user -> UserService, 设置成true: user -> IUserService
        generateByTables(serviceNameStartWithI, packageName, "erp_birth_card");
    }

    /**
     * @param serviceNameStartWithI 是否以I开头 user -> UserService, 设置成true: user -> IUserService
     * @param packageName           包名
     * @param tableNames            需要生成的表数组
     */
    private static void generateByTables(boolean serviceNameStartWithI, String packageName, String... tableNames) {
        GlobalConfig config = new GlobalConfig();
        DataSourceConfig dataSourceConfig = new DataSourceConfig();

        dataSourceConfig.setDbType(DbType.MYSQL)
                .setUrl("jdbc:mysql://192.168.172.50:6006/erp?autoReconnect=true&amp;amp;useUnicode=true&amp;amp;characterEncoding=utf8;useSSL=false;")
                .setUsername("dev_user")
                .setPassword("dev_password")
                .setDriverName("com.mysql.jdbc.Driver");


        StrategyConfig strategyConfig = new StrategyConfig();
        strategyConfig
                //需要生成代码的表前缀
                .setTablePrefix(new String[]{"erp_"})
                .setCapitalMode(true)
                .setEntityLombokModel(true)
//                .setDbColumnUnderline(true)
                .setColumnNaming(NamingStrategy.underline_to_camel)
                .setNaming(NamingStrategy.underline_to_camel)
                .setInclude(tableNames);//修改替换成你需要的表名，多个表名传数组
        config.setActiveRecord(false)
                .setAuthor(author)
                .setOutputDir(ourDir)
                .setFileOverride(true);
        if (!serviceNameStartWithI) {
            config.setServiceName("%sService");
        }
        TemplateConfig templateConfig = new TemplateConfig();
        new AutoGenerator().setGlobalConfig(config)
                .setDataSource(dataSourceConfig)
                .setTemplate(templateConfig)
                .setStrategy(strategyConfig)
                .setPackageInfo(
                        new PackageConfig()
                                .setParent(packageName)
                                .setController("controller")
                                .setEntity("domain")
                ).execute();
    }
}
