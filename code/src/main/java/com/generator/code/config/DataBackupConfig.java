package com.generator.code.config;

import org.springframework.context.annotation.Configuration;

import javax.annotation.PreDestroy;

/**
 * @Author suyanlong
 * @Description TODO
 * @Date 2021/5/18 20:56
 * @Version 1.0
 */
@Configuration
public class DataBackupConfig {

    @PreDestroy
    public void backData() {
        System.out.println("正在备份数据。。。。。。。。。。。");
    }
}
