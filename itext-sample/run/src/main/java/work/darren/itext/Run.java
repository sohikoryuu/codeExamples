package work.darren.itext;

import work.darren.itext.common.api.PdfGenerator;
import work.darren.itext5.generator.Itext5Generator;
import work.darren.itext7.generator.Itext7Generator;

/**
 * 测试用例
 *
 * @author darren
 * @date 2019-05-25
 */
public class Run {

    public static void main(String[] args) throws Exception {

        PdfGenerator itext5 = Itext5Generator.getInstanse();

        PdfGenerator itext7 = Itext7Generator.getInstanse();

        String savePath = "D:\\workspaceMY\\itext-sample\\itext-sample\\Volumes\\Transcend\\";//生成PDF的保存目录

        itext5.generatePdf(itext5.html(), savePath + "itext5.pdf");

        itext7.generatePdf(itext7.html(), savePath + "itext7.pdf");
    }
}
