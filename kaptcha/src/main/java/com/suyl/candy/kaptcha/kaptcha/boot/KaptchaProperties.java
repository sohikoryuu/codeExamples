/**
 * Copyright © 2018 TaoYu (tracy5546@gmail.com)
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.suyl.candy.kaptcha.kaptcha.boot;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

/**
 * 验证码组件参数
 *
 * @author TaoYu
 */
@ConfigurationProperties(prefix = "kaptcha")
public class KaptchaProperties {

    /**
     * 宽度
     */
    private Integer width = 200;
    /**
     * 高度
     */
    private Integer height = 50;
    /**
     * 内容
     */
    @NestedConfigurationProperty
    private Content content = new Content();
    /**
     * 背景色
     */
    @NestedConfigurationProperty
    private BackgroundColor backgroundColor = new BackgroundColor();
    /**
     * 字体
     */
    @NestedConfigurationProperty
    private Font font = new Font();
    /**
     * 边框
     */
    @NestedConfigurationProperty
    private Border border = new Border();

    static class BackgroundColor {

        /**
         * 开始渐变色
         */
        private String from = "lightGray";
        /**
         * 结束渐变色
         */
        private String to = "white";

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public String getTo() {
            return to;
        }

        public void setTo(String to) {
            this.to = to;
        }
    }

    static class Content {

        /**
         * 内容源
         */
        private String source = "abcdefghjklmnopqrstuvwxyz23456789";
        /**
         * 内容长度
         */
        private Integer length = 4;
        /**
         * 内容间隔
         */
        private Integer space = 2;

        public String getSource() {
            return source;
        }

        public void setSource(String source) {
            this.source = source;
        }

        public Integer getLength() {
            return length;
        }

        public void setLength(Integer length) {
            this.length = length;
        }

        public Integer getSpace() {
            return space;
        }

        public void setSpace(Integer space) {
            this.space = space;
        }
    }

    static class Border {

        /**
         * 是否开启
         */
        private Boolean enabled = true;
        /**
         * 颜色
         */
        private String color = "black";
        /**
         * 厚度
         */
        private Integer thickness = 1;

        public Boolean getEnabled() {
            return enabled;
        }

        public void setEnabled(Boolean enabled) {
            this.enabled = enabled;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public Integer getThickness() {
            return thickness;
        }

        public void setThickness(Integer thickness) {
            this.thickness = thickness;
        }
    }

    static class Font {

        /**
         * 名称
         */
        private String name = "Arial";
        /**
         * 颜色
         */
        private String color = "black";
        /**
         * 大小
         */
        private Integer size = 40;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public Integer getSize() {
            return size;
        }

        public void setSize(Integer size) {
            this.size = size;
        }
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }

    public BackgroundColor getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(BackgroundColor backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public Font getFont() {
        return font;
    }

    public void setFont(Font font) {
        this.font = font;
    }

    public Border getBorder() {
        return border;
    }

    public void setBorder(Border border) {
        this.border = border;
    }
}
