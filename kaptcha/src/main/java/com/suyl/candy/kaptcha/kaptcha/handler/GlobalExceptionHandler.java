package com.suyl.candy.kaptcha.kaptcha.handler;

import com.suyl.candy.kaptcha.kaptcha.exception.KaptchaException;
import com.suyl.candy.kaptcha.kaptcha.exception.KaptchaIncorrectException;
import com.suyl.candy.kaptcha.kaptcha.exception.KaptchaNotFoundException;
import com.suyl.candy.kaptcha.kaptcha.exception.KaptchaTimeoutException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = KaptchaException.class)
    public String kaptchaExceptionHandler(KaptchaException kaptchaException) {
        if (kaptchaException instanceof KaptchaIncorrectException) {
            return "验证码不正确";
        } else if (kaptchaException instanceof KaptchaNotFoundException) {
            return "验证码未找到";
        } else if (kaptchaException instanceof KaptchaTimeoutException) {
            return "验证码过期";
        } else {
            return "验证码渲染失败";
        }

    }
}
