package com.suyl.httpsdemo;

import com.suyl.httpsdemo.service.AsyncService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@EnableAsync
public class HttpsDemoApplication {

    @Autowired
    AsyncService asyncService;

    // https://mp.weixin.qq.com/s/JDxJFeC8p5z3JLiymoFwhA
    public static void main(String[] args) {
        SpringApplication.run(HttpsDemoApplication.class, args);
    }

    @GetMapping("suyl")
    public String getest() {
        return "test ok...";
    }

    @GetMapping("task")
    public String task(int num) throws Exception {
        long start = System.currentTimeMillis();

        for (int i = 0; i < num; i++) {
//            asyncService.doTaskOne();
//            Future<String> stringFuture = asyncService.getDataResult();
            asyncService.getDataResult();
        }
        long end = System.currentTimeMillis();
        System.out.println(Thread.currentThread().getName() + "主线程，执行时间：" + (end - start) + "ms");
        return "success";
    }

}
