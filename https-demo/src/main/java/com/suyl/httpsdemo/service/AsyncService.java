package com.suyl.httpsdemo.service;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

import java.util.Random;
import java.util.concurrent.Future;
import java.util.logging.Logger;

/**
 * @author suyanlong
 * @version 1.0
 * @createDate 2020/3/18 21:17
 */
@Component
public class AsyncService {
    Logger log = Logger.getLogger("AsyncService");
    public static Random random = new Random();

    @Async("asyncServiceExecutor")
    public Future<String> getDataResult() {
        long start = System.currentTimeMillis();
        System.out.println(Thread.currentThread().getName() + "[异步处理开始]");
        int result = random.nextInt(4000);
        try {
            Thread.sleep(result);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        long end = System.currentTimeMillis();
        System.out.println(Thread.currentThread().getName() + "【异步处理完成】" + (end - start) + "ms");
        return new AsyncResult<String>(String.valueOf(result));
    }

    @Async("asyncServiceExecutor")
    public void doTaskOne() throws Exception {
        log.info("开始做任务一");
        long start = System.currentTimeMillis();
        Thread.sleep(random.nextInt(4000));
        long end = System.currentTimeMillis();
        log.info("完成任务一，耗时：" + (end - start) + "毫秒");
    }
}
