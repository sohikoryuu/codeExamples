package com.suyl.httpsdemo;

import com.suyl.httpsdemo.service.AsyncService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class HttpsDemoApplicationTests {

    @Autowired
    AsyncService asyncService;

    @Test
    void contextLoads() throws Exception {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 20; i++) {
            asyncService.doTaskOne();
//            asyncService.getDataResult();
        }
        long end = System.currentTimeMillis();
        System.out.println(Thread.currentThread().getName() + "主线程，执行时间：" + (end - start) + "ms");
    }

}
