package com.suyl.markbfbc.strdemo;

/**
 * String 验证
 */
public class TestStrDemo {
    public static void main(String[] args) {

        String str1 = "what";
        String str2 = str1 + " a nice day";
        System.out.println("what a nice day".equals(str2)); // true
        System.out.println("what a nice day" == str2); // false

        String str11 = "what a nice day";
        String str21 = new String("what a nice day");
        System.out.println(str11.equals(str21)); // true
        System.out.println(str11 == str21); // false

        String str111 = "what";
        String str222 = str111.concat(" a nice day");
        System.out.println("what a nice day".equals(str222));  // true
        System.out.println("what a nice day" == str222); // false
        System.out.println("what a nice day" == str222.intern()); // false X true
    }
}
