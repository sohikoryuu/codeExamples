package com.suyl.markbfbc.test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * https://mp.weixin.qq.com/s/oaUJkS_BeYLxL3bSpCqLHg
 */
public class ThreadLocalTest {
    static class SeqCount {
        private static ThreadLocal<Integer> integerThreadLocal = new ThreadLocal<Integer>() {
            @Override
            protected Integer initialValue() {
                return 0;
            }
        };

        public int nextSeq() {
            integerThreadLocal.set(integerThreadLocal.get() + 1);
            return integerThreadLocal.get();
        }

        public static void main(String[] args) {
            SeqCount seqCount = new SeqCount();
            SeqThread seqThread1 = new SeqThread(seqCount);
            SeqThread seqThread2 = new SeqThread(seqCount);
            SeqThread seqThread3 = new SeqThread(seqCount);
            SeqThread seqThread4 = new SeqThread(seqCount);
            seqThread1.start();
            seqThread2.start();
            seqThread3.start();
            seqThread4.start();
        }

        public static class SeqThread extends Thread {
            private SeqCount seqCount;

            public SeqThread(SeqCount seqCount) {
                this.seqCount = seqCount;
            }

            @Override
            public void run() {
                for (int i = 0; i < 4; i++) {
                    System.out.println(Thread.currentThread().getName() + " SeqCount:" + seqCount.nextSeq());
                }
            }
        }
    }

    public static class SimpleDateFormatDemo {

        private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

        private static ThreadLocal<DateFormat> threadLocal = new ThreadLocal<>();

        /**
         * 获取线程的变量副本，如果不覆盖initialValue方法，第一次get将返回null,故需要创建一个DateFormat，放入threadLocal中
         *
         * @return
         */
        public DateFormat getDateFormat() {
            DateFormat df = threadLocal.get();
            if (df == null) {
                df = new SimpleDateFormat(DATE_FORMAT);
                threadLocal.set(df);
            }
            return df;
        }

        public static void main(String[] args) {
            SimpleDateFormatDemo formatDemo = new SimpleDateFormatDemo();

            MyRunnable myRunnable1 = new MyRunnable(formatDemo);
            MyRunnable myRunnable2 = new MyRunnable(formatDemo);
            MyRunnable myRunnable3 = new MyRunnable(formatDemo);

            Thread thread1 = new Thread(myRunnable1);
            Thread thread2 = new Thread(myRunnable2);
            Thread thread3 = new Thread(myRunnable3);
            thread1.start();
            thread2.start();
            thread3.start();
        }


        public static class MyRunnable implements Runnable {

            private SimpleDateFormatDemo dateFormatDemo;

            public MyRunnable(SimpleDateFormatDemo dateFormatDemo) {
                this.dateFormatDemo = dateFormatDemo;
            }

            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName() + " 当前时间：" + dateFormatDemo.getDateFormat().format(new Date()));
            }
        }
    }

}
