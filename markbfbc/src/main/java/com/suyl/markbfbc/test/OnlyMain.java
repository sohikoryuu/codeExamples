package com.suyl.markbfbc.test;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;

/**
 * 仅仅一个Main函数启动多少线程
 */
public class OnlyMain {

    public static void main(String[] args) {
        // java 虚拟机线程系统的管理接口
        ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
        // 仅仅获取线程和线程堆栈信息
        ThreadInfo[] threadInfos =
                threadMXBean.dumpAllThreads(false, false);
        // 遍历线程信息，打印线程ID和线程名称
        for (ThreadInfo threadInfo : threadInfos) {
            System.out.println("【" + threadInfo.getThreadId() + ":" + threadInfo.getThreadName() + "】");
        }
    }
}
