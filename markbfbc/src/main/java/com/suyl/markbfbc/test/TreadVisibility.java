package com.suyl.markbfbc.test;

/**
 * 线程间可见性，volatile 可见
 */
public class TreadVisibility {

    private static volatile boolean flag = true;

    public static void main(String[] args) throws InterruptedException {
        new Thread(() -> {
            while (flag) {

            }
            System.out.println("end");
        }, "server").start();

        Thread.sleep(1000);

        flag = false;
    }
}
