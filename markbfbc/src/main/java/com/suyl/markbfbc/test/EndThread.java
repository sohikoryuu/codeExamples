package com.suyl.markbfbc.test;

/**
 * 线程正确停止的姿势
 */
public class EndThread {

    public static class UseThread extends Thread {

        public UseThread(String name) {
            super(name);
        }

        @Override
        public void run() {
            String threadName = Thread.currentThread().getName();
            System.out.println("thread name = " + threadName + "  " + isInterrupted() + "  " + interrupted());
            while (!isInterrupted()) {
//            while (!Thread.interrupted()) {
//            while (true) {
                System.out.println("thread name = " + threadName + "  " + isInterrupted() + "  " + interrupted());
            }
            System.out.println("thread name = " + threadName + "  " + isInterrupted() + "  " + interrupted());
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Thread useThread = new UseThread(" end Thread");
        useThread.start();
        Thread.sleep(5);
        useThread.interrupt();
    }
}
