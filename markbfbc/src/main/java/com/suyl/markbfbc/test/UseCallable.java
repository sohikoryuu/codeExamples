package com.suyl.markbfbc.test;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * Callable 使用
 */
public class UseCallable {

    /**
     * 实现Callable接口，允许有返回值
     */
    private static class UseCallables implements Callable<Integer> {
        private int sum;

        @Override
        public Integer call() throws Exception {
            System.out.println("子线程开始计算：");
            for (int i = 0; i < 500; i++) {
                sum += i;
//                System.out.println(sum);
            }
            System.out.println("子线程计算结束，结果为：" + sum);
            return sum;
        }
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        UseCallables useCallables = new UseCallables();
        // 包装
        FutureTask<Integer> futureTask = new FutureTask<>(useCallables);
        new Thread(futureTask).start();
        System.out.println("执行结果为：" + futureTask.get());
    }
}
