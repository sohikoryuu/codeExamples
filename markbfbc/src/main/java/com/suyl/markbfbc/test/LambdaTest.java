package com.suyl.markbfbc.test;

import java.util.ArrayList;
import java.util.List;

/**
 * lambda表达式测试性能
 */
public class LambdaTest {

    public static void main(String[] args) throws InterruptedException {

        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            list.add(i);
        }

        long lambdaStart = System.currentTimeMillis();
        list.forEach(i -> {
            // 不用做事情，循环就够了
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        long lambdaEnd = System.currentTimeMillis();
        System.out.println("lambda循环运行毫秒数===" + (lambdaEnd - lambdaStart));

        long normalStart = System.currentTimeMillis();
        for (int i = 0; i < list.size(); i++) {
            // 不用做事情，循环就够了
            Thread.sleep(1);
        }
        long normalEnd = System.currentTimeMillis();
        System.out.println("普通循环运行毫秒数===" + (normalEnd - normalStart));


        long lambdaStreamStart = System.currentTimeMillis();
        list.stream().filter(i -> {
            // 不用做事情，循环就够了
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return true;
        });
        long lambdaStreamEnd = System.currentTimeMillis();
        System.out.println("lambdaStream循环运行毫秒数===" + (lambdaStreamEnd - lambdaStreamStart));
        long lambdaParallelStreamStart = System.currentTimeMillis();
        list.stream().parallel().filter(i -> {
            // 不用做事情，循环就够了
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return true;
        });
        long lambdaParallelStreamEnd = System.currentTimeMillis();
        System.out.println("lambdaParallelStream循环运行毫秒数===" + (lambdaParallelStreamEnd - lambdaParallelStreamStart));
    }
}
