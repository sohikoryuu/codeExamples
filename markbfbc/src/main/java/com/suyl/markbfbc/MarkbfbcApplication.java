package com.suyl.markbfbc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class MarkbfbcApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarkbfbcApplication.class, args);
    }

    @GetMapping("suyl")
    @ResponseBody
    public String get() {
        return "这是测试https协议";
    }
}
