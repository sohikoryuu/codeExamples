package com.suyl.markbfbc.kafka;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * kafka 生产者
 */
@Component
public class KafkaProducer {
    private static Logger logger = LoggerFactory.getLogger(KafkaProducer.class);

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplates;

    private Gson gson = new GsonBuilder().create();

    //发送消息方法
    public void send() {
        for (int i = 0; i < 5; i++) {
            Map message = new HashMap();
            message.put("date", System.currentTimeMillis());
            message.put("uuid", UUID.randomUUID().toString() + "---" + i);
            message.put("sendtime", new Date());
            logger.info("发送消息 ----->>>>>  message = {}", gson.toJson(message));
            kafkaTemplates.send("hello", message.toString());
        }
    }
}
