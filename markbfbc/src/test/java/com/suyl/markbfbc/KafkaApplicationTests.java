package com.suyl.markbfbc;

import com.suyl.markbfbc.kafka.KafkaProducer;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * kafka 发送topic
 */
@SpringBootTest
class KafkaApplicationTests {

    @Autowired
    KafkaProducer kafkaProducer;

    @Test
    void contextLoads() throws InterruptedException {
        kafkaProducer.send();
    }
}
