package com.suyl.markbfbc;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.*;

/**
 * 1.模拟高并发场景 1000 QPS/s
 * 2.高并发场景优化
 */
@SpringBootTest
class MarkbfbcApplicationTests {

    private static final int MAX_THREADS = 1000;

    private static CountDownLatch countDownLatch = new CountDownLatch(MAX_THREADS);

    /**
     * 模拟多并发请求
     *
     * @throws InterruptedException
     */
    @Test
    void contextLoads() throws InterruptedException {

        for (int i = 0; i < MAX_THREADS; i++) {
            Thread thread = new Thread(() -> {
                try {
                    countDownLatch.countDown();
                    countDownLatch.await();
//                    System.out.println(123456);
                    queryOrderInfo("123456");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            });
            thread.start();
        }
        Thread.sleep(2000);
    }

    // ============================xing能优化 ===========================
    class Request {
        String orderCode;
        String serialNo;
        CompletableFuture<Map<String, Object>> future;
    }

    LinkedBlockingQueue<Request> queue = new LinkedBlockingQueue<>();

    // 1000个线程调用
    public Map<String, Object> queryOrderInfo(String orderCode) throws ExecutionException, InterruptedException {
        String serialNo = UUID.randomUUID().toString();
        Request request = new Request();
        request.orderCode = orderCode;
        request.serialNo = serialNo;

        // 把结果通知到各个线程，对每个线程都分配一个
        // jdk1.8 CompletableFuture 完成分
        CompletableFuture<Map<String, Object>> future = new CompletableFuture<>();
        request.future = future;  // jiangjieguo fanhui dao duiying 线程，根据serialNo 找到对饮线程

        queue.add(request);

        return future.get(); // 阻塞
    }

    @PostConstruct
    public void init() {
        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);
        scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                int size = queue.size();
                if (size == 0)
                    return;

                ArrayList<Request> requests = new ArrayList<>();
                for (int i = 0; i < size; i++) {
                    Request request = queue.poll();
                    requests.add(request);
                }

                System.out.println("批量处理的数据量为：" + size);

                List<Map<String, String>> params = new ArrayList<>();
                for (Request request : requests) {
                    Map<String, String> map = new HashMap<>();
                    map.put("orderCOde", request.orderCode);
                    map.put("serialNo", request.serialNo);
                    params.add(map);
                }
                // 远程调用
                // TODO 这里调用远程接口、假设返回的结果如下
                List<Map<String, Object>> responses = new ArrayList<>();
                HashMap<String, Map<String, Object>> responseMap = new HashMap<>();
                // genju serialNo 区别是哪个线程
                for (Map<String, Object> response : responses) {
                    String serialNo = response.get("serialNo").toString();
                    responseMap.put(serialNo, response);
                }

                for (Request request : requests) {
                    Map<String, Object> result = responseMap.get(request.serialNo);
                    request.future.complete(result);
                }

            }
        }, 0, 10, TimeUnit.MILLISECONDS);
    }
}
