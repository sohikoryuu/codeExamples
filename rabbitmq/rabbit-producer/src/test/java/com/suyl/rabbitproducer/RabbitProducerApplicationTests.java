package com.suyl.rabbitproducer;

import com.suyl.rabbitproducer.producer.RabbitProducer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RabbitProducerApplicationTests {

    @Autowired
    RabbitProducer rabbitProducer;

    /**
     * 1.普通工作队列模式
     */
    @Test
    public void testSendRabbitMq() {
        for (int i = 0; i < 10; i++) {
            rabbitProducer.stringSend();
        }
    }

    /**
     * 2.fanout 模式 (广播模式)
     */
    @Test
    public void testFanoutSend() {
        rabbitProducer.fanoutSend();
    }

    /**
     * 3.topic 模式
     */
    @Test
    public void testTopic() {
        rabbitProducer.topicTopic1Send();
        rabbitProducer.topicTopic2Send();
        rabbitProducer.topicTopic3Send();
    }

}
