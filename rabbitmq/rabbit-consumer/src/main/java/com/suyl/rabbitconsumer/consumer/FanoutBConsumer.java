package com.suyl.rabbitconsumer.consumer;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author suyl
 * @date 2019/08/12
 * @description 2.fanout 模式 (广播模式)
 */
@Component
@RabbitListener(queues = "fanout.b")
public class FanoutBConsumer {
    @Autowired
    AmqpTemplate amqpTemplate;

    /**
     * 消息消费
     *
     * @param msg
     * @RabbitHandler 代表此方法为接受到消息后的处理方法
     */
    @RabbitHandler
    public void recieved(String msg) {
        System.out.println("[fanout.b] recieved message:" + msg);
    }
}
