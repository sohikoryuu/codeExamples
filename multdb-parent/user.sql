/*
 Navicat Premium Data Transfer

 Source Server         : root@192.168.56.1
 Source Server Type    : MySQL
 Source Server Version : 50719
 Source Host           : 192.168.56.1:3307
 Source Schema         : springbootdb

 Target Server Type    : MySQL
 Target Server Version : 50719
 File Encoding         : 65001

 Date: 02/06/2018 21:48:37
*/

create database springbootdb character set 'utf8' collate 'utf8_general_ci';

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '用户编号',
  `user_name` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户名称',
  `description` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, '泥瓦匠', '他有一个小网站 bysocket.com');
INSERT INTO `user` VALUES (2, '张三', '经常出现的一个人');
INSERT INTO `user` VALUES (3, 'User0', 'Desc0');
INSERT INTO `user` VALUES (4, 'User1', 'Desc1');
INSERT INTO `user` VALUES (5, 'User2', 'Desc2');
INSERT INTO `user` VALUES (6, 'User3', 'Desc3');
INSERT INTO `user` VALUES (7, 'User4', 'Desc4');
INSERT INTO `user` VALUES (8, 'User5', 'Desc5');
INSERT INTO `user` VALUES (9, 'User6', 'Desc6');
INSERT INTO `user` VALUES (10, 'User7', 'Desc7');
INSERT INTO `user` VALUES (11, 'User8', 'Desc8');
INSERT INTO `user` VALUES (12, 'User9', 'Desc9');
INSERT INTO `user` VALUES (13, 'User10', 'Desc10');
INSERT INTO `user` VALUES (14, 'User11', 'Desc11');
INSERT INTO `user` VALUES (15, 'User12', 'Desc12');
INSERT INTO `user` VALUES (16, 'User13', 'Desc13');
INSERT INTO `user` VALUES (17, 'User14', 'Desc14');
INSERT INTO `user` VALUES (18, 'User15', 'Desc15');
INSERT INTO `user` VALUES (19, 'User16', 'Desc16');
INSERT INTO `user` VALUES (20, 'User17', 'Desc17');
INSERT INTO `user` VALUES (21, 'User18', 'Desc18');
INSERT INTO `user` VALUES (22, 'User19', 'Desc19');
INSERT INTO `user` VALUES (23, '李四', '另一个常见的用户');

SET FOREIGN_KEY_CHECKS = 1;
