/*
 Navicat Premium Data Transfer

 Source Server         : root@192.168.56.1
 Source Server Type    : MySQL
 Source Server Version : 50719
 Source Host           : 192.168.56.1:3307
 Source Schema         : springbootdb_cluster

 Target Server Type    : MySQL
 Target Server Version : 50719
 File Encoding         : 65001

 Date: 02/06/2018 21:49:04
*/

create database springbootdb_cluster character set 'utf8' collate 'utf8_general_ci';

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for city
-- ----------------------------
DROP TABLE IF EXISTS `city`;
CREATE TABLE `city`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '城市编号',
  `province_id` int(10) UNSIGNED NOT NULL COMMENT '省份编号',
  `city_name` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '城市名称',
  `description` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of city
-- ----------------------------
INSERT INTO `city` VALUES (1, 1, '温岭市', 'BYSocket 的家在温岭。');
INSERT INTO `city` VALUES (2, 2, '重庆', '雾都，山城');
INSERT INTO `city` VALUES (3, 3, '上海', '魔都');

SET FOREIGN_KEY_CHECKS = 1;
