package com.multdb.aop;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@MapperScan("com.mriassl.multdb.mapper")
@SpringBootApplication
//@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class AopApplication implements CommandLineRunner{
	
	public static void main(String[] args) {
		new SpringApplicationBuilder(AopApplication.class).web(true).run(args);
	}
	
	@Override
	public void run(String... args) throws Exception {
		//启动web
		System.out.println("MultdbApplication server is running!");
	}
	
}
