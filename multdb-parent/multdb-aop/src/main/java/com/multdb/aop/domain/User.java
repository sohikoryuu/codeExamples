package com.multdb.aop.domain;

import java.io.Serializable;

import javax.persistence.Id;
import javax.persistence.Table;

import tk.mybatis.mapper.annotation.KeySql;
import tk.mybatis.mapper.code.IdentityDialect;

@Table(name = "user")
public class User implements Serializable {
	private static final long serialVersionUID = 7145582821847748939L;

	@Id
	@KeySql(useGeneratedKeys = true, dialect = IdentityDialect.MYSQL)
	private Long id;
	private String userName;
	private String description;
	private City city;

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
