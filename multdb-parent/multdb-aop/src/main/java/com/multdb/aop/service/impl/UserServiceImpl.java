package com.multdb.aop.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.multdb.aop.config.BaseMapper;
import com.multdb.aop.config.dbconfig.DBTypeEnum;
import com.multdb.aop.config.dbconfig.TargetDataSourceAnno;
import com.multdb.aop.domain.City;
import com.multdb.aop.domain.User;
import com.multdb.aop.mapper.UserMapper;
import com.multdb.aop.service.CityService;
import com.multdb.aop.service.UserService;

@Service
public class UserServiceImpl extends BaseServiceImpl<User> implements UserService {

    @Autowired
    private UserMapper userMapper; // 主数据源

    @Autowired
    private CityService cityService; // 从数据源

    @Override
	protected BaseMapper<User> getMapper() {
		return userMapper;
	}
    
    @Override
    @TargetDataSourceAnno(DBTypeEnum.DB_ONE)
	public void saveUser(User user) {
    	userMapper.insert(user);
	}
    
    @Override
    @TargetDataSourceAnno(DBTypeEnum.DB_ONE)
    public User findByName(String userName) {
        User user = userMapper.findByName(userName);
        City city = cityService.findByName("重庆");
        user.setCity(city);
        return user;
    }

}
