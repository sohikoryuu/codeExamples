package com.multdb.aop.mapper;

import org.apache.ibatis.annotations.Param;

import com.multdb.aop.config.BaseMapper;
import com.multdb.aop.domain.User;

public interface UserMapper extends BaseMapper<User> {

    User findByName(@Param("userName") String userName);
}
