package com.multdb.aop.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.multdb.aop.config.BaseMapper;
import com.multdb.aop.config.dbconfig.DBTypeEnum;
import com.multdb.aop.config.dbconfig.TargetDataSourceAnno;
import com.multdb.aop.domain.City;
import com.multdb.aop.mapper.CityMapper;
import com.multdb.aop.service.CityService;

@Service
public class CityServiceImpl extends BaseServiceImpl<City> implements CityService {

    @Autowired
    private CityMapper cityMapper; // 从数据源

    @Override
	protected BaseMapper<City> getMapper() {
		return cityMapper;
	}

	@Override
	@TargetDataSourceAnno(DBTypeEnum.DB_TWO)
	public void saveCity(City city) {
		cityMapper.insert(city);
	}

	@Override
	@TargetDataSourceAnno(DBTypeEnum.DB_TWO)
	public City findByName(String cityName) {
		return cityMapper.findByName(cityName);
	}

}
