package com.multdb.aop.service;

import com.multdb.aop.domain.City;

public interface CityService extends BaseService<City> {

	void saveCity(City city);
	
	City findByName(String cityName);
}
