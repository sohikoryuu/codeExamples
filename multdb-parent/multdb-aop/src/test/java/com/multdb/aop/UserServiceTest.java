package com.multdb.aop;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.multdb.aop.config.dbconfig.DBContextHolder;
import com.multdb.aop.config.dbconfig.DBTypeEnum;
import com.multdb.aop.domain.City;
import com.multdb.aop.domain.User;
import com.multdb.aop.mapper.CityMapper;
import com.multdb.aop.service.UserService;
import com.multdb.aop.util.DataSourceSwitcher;
import com.multdb.aop.util.DataSourceSwitcher.CallBack;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {
	
	@Autowired
	private UserService userService;

	@Autowired
	private CityMapper cityMapper;
	
	@Test
	public void saveUsersTest(){
		DBContextHolder.setDBType(DBTypeEnum.DB_TWO);
		City city = new City();
		city.setCityName("重庆");
		city.setProvinceId(2L);
		city.setDescription("雾都，山城");
		cityMapper.insert(city);
		
		DBContextHolder.setDBType(DBTypeEnum.DB_ONE);
		int count = userService.getCountByCondition(null);
		
		List<User> users = new ArrayList<>();
		User user = null;
		for(int i=10;i<20;i++){
			user = new User();
			user.setUserName("User"+i);
			user.setDescription("Desc"+i);
			users.add(user);
		}
		
		userService.save(users);
		int count2 = userService.getCountByCondition(null);
		DBContextHolder.clearDBType();
		
		Assert.assertTrue(count2 > count);
	}

	@Test
	public void selectUserTest(){
		User user = userService.findByName("张三");
		City city = user.getCity();
		System.out.println(String.format("%s %s %s", user.getUserName(),user.getDescription(),city==null?"null":city.getCityName()));
		Assert.assertNotNull(user);
	}

	@Test
	public void switcherTest(){
		City city = new City();
		city.setCityName("上海");
		city.setProvinceId(3L);
		city.setDescription("魔都");
		DataSourceSwitcher.execute(DBTypeEnum.DB_TWO, new CallBack() {
			
			@Override
			public void run() {
				cityMapper.insert(city);
			}
		});
		User user =  new User();
		user.setUserName("李四");
		user.setDescription("另一个常见的用户");
		DataSourceSwitcher.execute(DBTypeEnum.DB_ONE, new CallBack() {
			
			@Override
			public void run() {
				userService.save(user);
			}
		});
	}
}
