package com.multdb.aop;

import com.multdb.aop.config.dbconfig.DBTypeEnum;
import com.multdb.aop.config.dbconfig.TargetDataSourceAnno;
import com.multdb.aop.domain.User;
import com.multdb.aop.mapper.CityMapper;
import com.multdb.aop.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private UserService userService;

    @Test
    public void get() {
        User user1 = userService.getById("1");
        logger.info("读取" + user1.getUserName());
        User user2 = userService.getById("1");
        logger.info("读取" + user2.getUserName());
        User user3 = userService.getById("1");
        logger.info("读取" + user3.getUserName());
        User user4 = userService.getById("1");
        logger.info("读取" + user4.getUserName());
    }

    @Test
    @TargetDataSourceAnno(DBTypeEnum.DB_SLAVE1)
    public void save() {
        User user = new User();
        user.setUserName("写入操作");
        user.setDescription("我是写入，我要存到master");
        int result = userService.save(user);
        logger.info("保存结果：" + result);
    }
}
