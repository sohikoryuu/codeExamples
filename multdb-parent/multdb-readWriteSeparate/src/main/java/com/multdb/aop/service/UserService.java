package com.multdb.aop.service;

import com.multdb.aop.domain.User;

public interface UserService extends BaseService<User> {

	void saveUser(User user);
	
    User findByName(String userName);

    User getById(String id);
}
