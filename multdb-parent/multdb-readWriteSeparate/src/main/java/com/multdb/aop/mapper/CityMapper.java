package com.multdb.aop.mapper;

import org.apache.ibatis.annotations.Param;

import com.multdb.aop.config.BaseMapper;
import com.multdb.aop.domain.City;

public interface CityMapper extends BaseMapper<City> {

	City findByName(@Param("cityName") String cityName);
}
