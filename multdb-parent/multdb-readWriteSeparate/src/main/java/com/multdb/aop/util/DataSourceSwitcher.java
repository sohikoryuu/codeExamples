package com.multdb.aop.util;

import com.multdb.aop.config.dbconfig.DBContextHolder;
import com.multdb.aop.config.dbconfig.DBTypeEnum;

public class DataSourceSwitcher {

	public static void execute(DBTypeEnum dbType, CallBack callBack){
		DBContextHolder.setDBType(dbType);
		callBack.run();
		DBContextHolder.clearDBType();
	}
	
	public static abstract class CallBack {
		public abstract void run();
	}
	
}
