package com.multdb.aop.config.dbconfig;

import java.util.concurrent.atomic.AtomicInteger;

public class DBContextHolder {
    // 当前线程存入当前数据源信息
    private static final ThreadLocal<DBTypeEnum> contextHolder = new ThreadLocal<DBTypeEnum>();

    // 改变当前数据源
    public static void setDBType(DBTypeEnum dbType) {
        contextHolder.set(dbType);
    }

    // 获取当前数据源
    public static DBTypeEnum getDBType() {
        return contextHolder.get();
    }

    public static void clearDBType() {
        contextHolder.remove();
    }

    // ###########################################
    private static final AtomicInteger counter = new AtomicInteger(-1);

    public static void set(DBTypeEnum dbType) {
        contextHolder.set(dbType);
    }

    public static DBTypeEnum get() {
        return contextHolder.get();
    }

    public static void master() {
        set(DBTypeEnum.DB_MASTER);
        System.out.println("切换到master");
    }

    public static void slave() {
        //  轮询
        int index = counter.getAndIncrement() % 2;
        if (counter.get() > 9999) {
            counter.set(-1);
        }
        if (index == 0) {
            set(DBTypeEnum.DB_SLAVE1);
            System.out.println("切换到slave1");
        } else {
            set(DBTypeEnum.DB_SLAVE2);
            System.out.println("切换到slave2");
        }
    }
}