package com.multdb.aop.domain;

import java.io.Serializable;

import javax.persistence.Id;
import javax.persistence.Table;

import tk.mybatis.mapper.annotation.KeySql;
import tk.mybatis.mapper.code.IdentityDialect;

@Table(name = "city")
public class City implements Serializable {

	private static final long serialVersionUID = -7286384027584580462L;

	@Id
	@KeySql(useGeneratedKeys = true, dialect = IdentityDialect.MYSQL)
	private Long id;
	private Long provinceId;
	private String cityName;
	private String description;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(Long provinceId) {
		this.provinceId = provinceId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
