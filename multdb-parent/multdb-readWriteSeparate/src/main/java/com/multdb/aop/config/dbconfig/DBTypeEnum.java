package com.multdb.aop.config.dbconfig;

public enum DBTypeEnum {
    DB_MASTER, DB_SLAVE1, DB_SLAVE2;
}