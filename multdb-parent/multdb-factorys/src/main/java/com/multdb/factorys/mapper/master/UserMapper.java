package com.multdb.factorys.mapper.master;

import org.apache.ibatis.annotations.Param;

import com.multdb.factorys.domain.User;
import com.multdb.factorys.mapper.BaseMapper;

public interface UserMapper extends BaseMapper<User> {

    /**
     * 根据用户名获取用户信息
     * @param userName
     * @return
     */
    User findByName(@Param("userName") String userName);
}
