package com.multdb.factorys.web;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageHelper;
import com.multdb.factorys.domain.User;
import com.multdb.factorys.service.UserService;

@RestController
@RequestMapping("/test")
public class TestController{

    @Resource
    private UserService userService;
    
    @GetMapping("/run")
    public List<User> getUsers(HttpServletRequest request) {
    	PageHelper.startPage(request);
        return userService.getAll();
    }

}
