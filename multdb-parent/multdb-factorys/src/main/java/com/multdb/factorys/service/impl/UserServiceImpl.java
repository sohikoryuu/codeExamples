package com.multdb.factorys.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.multdb.factorys.domain.City;
import com.multdb.factorys.domain.User;
import com.multdb.factorys.mapper.BaseMapper;
import com.multdb.factorys.mapper.cluster.CityMapper;
import com.multdb.factorys.mapper.master.UserMapper;
import com.multdb.factorys.service.UserService;

@Service
public class UserServiceImpl extends BaseServiceImpl<User> implements UserService {

    @Autowired
    private UserMapper userMapper; // 主数据源

    @Autowired
    private CityMapper cityMapper; // 从数据源

    @Override
	protected BaseMapper<User> getMapper() {
		return userMapper;
	}
    
    @Override
	public void saveUser(User user) {
    	userMapper.insert(user);
	}
    
    @Override
    public User findByName(String userName) {
        User user = userMapper.findByName(userName);
        City city = cityMapper.findByName("温岭市");
        user.setCity(city);
        return user;
    }

}
