package com.multdb.factorys;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class FactoryApplication implements CommandLineRunner{
	
	public static void main(String[] args) {
		new SpringApplicationBuilder(FactoryApplication.class).web(true).run(args);
	}
	
	@Override
	public void run(String... args) throws Exception {
		//启动web
		System.out.println("MultdbApplication server is running!");
	}
	
}
