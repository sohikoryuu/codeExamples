package com.multdb.factorys.mapper.cluster;

import org.apache.ibatis.annotations.Param;

import com.multdb.factorys.domain.City;
import com.multdb.factorys.mapper.BaseMapper;

public interface CityMapper extends BaseMapper<City> {

	/**
	 * 根据城市名称，查询城市信息
	 * @param cityName 城市名
	 */
	City findByName(@Param("cityName") String cityName);
}
