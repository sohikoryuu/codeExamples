package com.multdb.factorys.service;

import com.multdb.factorys.domain.User;

public interface UserService extends BaseService<User> {

	void saveUser(User user);
	
    User findByName(String userName);
}
