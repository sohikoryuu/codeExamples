package com.multdb.factorys;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.multdb.factorys.domain.City;
import com.multdb.factorys.domain.User;
import com.multdb.factorys.service.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {
	
	@Autowired
	private UserService userService;
	
	@Test
	public void saveUserTest(){
	    User user = new User();
		user.setUserName("张三");
		user.setDescription("经常出现的一个人");
		userService.saveUser(user);
		
		User user2 = userService.findByName("张三");
		Assert.assertNotNull(user2);
	}

	@Test
	public void saveUsersTest(){
		int count = userService.getCountByCondition(null);
		
		List<User> users = new ArrayList<>();
		User user = null;
		for(int i=0;i<10;i++){
			user = new User();
			user.setUserName("User"+i);
			user.setDescription("Desc"+i);
			users.add(user);
		}
		userService.save(users);
		
		int count2 = userService.getCountByCondition(null);
		Assert.assertTrue(count2 > count);
	}

	@Test
	public void selectUserTest(){
		User user = userService.findByName("泥瓦匠");
		City city = user.getCity();
		System.out.println(String.format("%s %s %s", user.getUserName(),user.getDescription(),city==null?"null":city.getCityName()));
		Assert.assertNotNull(user);
	}
}
