package com.suyl.designpattern.ad_observerpattern.test;

import com.suyl.designpattern.ad_observerpattern.observer.Observer;
import com.suyl.designpattern.ad_observerpattern.observer.impl.User;
import com.suyl.designpattern.ad_observerpattern.observer.impl.WechatServer;

public class Test {

    public static void main(String[] args) {
        WechatServer server = new WechatServer();

        Observer userZhang = new User("ZhangSan");
        Observer userLi = new User("LiSi");
        Observer userWang = new User("WangWu");

        server.registerObserver(userZhang);
        server.registerObserver(userLi);
        server.registerObserver(userWang);
        server.setInfomation("java是世界上使用最广泛的语言！！");

        System.out.println("----------------------------------------------");
        server.removeObserver(userZhang);
        server.setInfomation("python是世界上最好用的语言！");

    }
}
