package com.suyl.designpattern.ad_observerpattern.observer.impl;

import com.suyl.designpattern.ad_observerpattern.observer.Observer;
import com.suyl.designpattern.ad_observerpattern.observer.Observerable;

import java.util.ArrayList;
import java.util.List;

public class WechatServer implements Observerable {

    private List<Observer> list;
    private String msg;

    public WechatServer() {
        list = new ArrayList<>();
    }

    @Override
    public void registerObserver(Observer observer) {
        list.add(observer);
    }

    @Override
    public void removeObserver(Observer observer) {
        if (!list.isEmpty()) {
            list.remove(observer);
        }
    }

    @Override
    public void notifyObserver() {
//        list.stream().filter(s -> {
//            s.update(msg);
//            return true;
//        }).collect(Collectors.toList());
        int size = list.size();
        for (int i = 0; i < size; i++) {
            list.get(i).update(msg);
        }
    }

    public void setInfomation(String msg) {
        this.msg = msg;
        System.out.println("微信服务更新的消息为：" + msg);
        notifyObserver();
    }
}
