package com.suyl.designpattern.ad_observerpattern.observer;

public interface Observer {

    public void update(String msg);
}
