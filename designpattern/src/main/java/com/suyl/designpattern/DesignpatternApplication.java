package com.suyl.designpattern;

import com.suyl.designpattern.ab_strategypattern.eg.ResultHandle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

@SpringBootApplication
public class DesignpatternApplication {
    @Autowired
    private Environment environment;
    public static void main(String[] args) {
        SpringApplication.run(DesignpatternApplication.class, args);

        ResultHandle rh = new ResultHandle();
        //第一种方式，运行成功
        rh.handleDeviceStatusStrategy("1111111", "1500", new byte[0]);
        //第二种方式，也运行成功
        rh.handleStrategy("1111111", "2100", new byte[0]);
    }

}
