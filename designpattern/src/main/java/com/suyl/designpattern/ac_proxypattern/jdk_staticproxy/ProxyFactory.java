package com.suyl.designpattern.ac_proxypattern.jdk_staticproxy;

/**
 * 产生代理对象的静态类工厂
 */
public class ProxyFactory {
    public static ILawSuit getProxy() {
        return new ProxyLawyer(new SecondDongWang());
    }
}
