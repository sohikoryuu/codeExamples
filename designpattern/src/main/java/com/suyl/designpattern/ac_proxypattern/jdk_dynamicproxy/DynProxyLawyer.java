package com.suyl.designpattern.ac_proxypattern.jdk_dynamicproxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * 动态代理类
 */
public class DynProxyLawyer implements InvocationHandler {

    private Object object; // 被代理对象

    public DynProxyLawyer(Object object) {
        this.object = object;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        System.out.println("案件进展：" + method.getName());
        Object result = method.invoke(object, args);
        return result;
    }
}
