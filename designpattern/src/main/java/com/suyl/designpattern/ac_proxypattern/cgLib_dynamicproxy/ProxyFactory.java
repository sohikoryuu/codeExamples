package com.suyl.designpattern.ac_proxypattern.cgLib_dynamicproxy;

import org.springframework.cglib.proxy.Enhancer;

public class ProxyFactory {
    public static Object getGcLibDynProxy(Object target) {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(target.getClass());
        enhancer.setCallback(new cgLibDynamicProxy());
        Object targetProxy = enhancer.create();
        return targetProxy;
    }
}
