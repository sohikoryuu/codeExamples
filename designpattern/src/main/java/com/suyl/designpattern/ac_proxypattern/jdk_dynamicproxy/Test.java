package com.suyl.designpattern.ac_proxypattern.jdk_dynamicproxy;

/**
 * 动态代理 测试类
 * <p>
 * 在java的动态代理机制中，有两个重要的类或接口，一个是InvocationHandler接口、另一个则是 Proxy类，这个类和接口是实现我们动态代理所必须用到的。
 * InvocationHandler接口是给动态代理类实现的，负责处理被代理对象的操作的，而Proxy是用来创建动态代理类实例对象的，因为只有得到了这个
 * 对象我们才能调用那些需要代理的方法。
 */
public class Test {

    public static void main(String[] args) {
        ILawSuit proxy = (ILawSuit) ProxyFactory.getProxy(new SeconDongWang());
        proxy.submit("工资流水再此！！");
        proxy.defend();
    }
}
