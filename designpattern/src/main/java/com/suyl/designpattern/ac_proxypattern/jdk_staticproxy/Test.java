package com.suyl.designpattern.ac_proxypattern.jdk_staticproxy;

/**
 * 测试类 静态代理
 * <p>
 * 静态代理是指预先确定了代理与被代理者的关系
 * 那映射到编程领域的话，就是指代理类与被代理类的依赖关系在编译期间就确定了
 */
public class Test {
    public static void main(String[] args) {
        ProxyFactory.getProxy().submit("工资流水在这儿");
        ProxyFactory.getProxy().defend();
    }
}
