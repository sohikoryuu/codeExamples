package com.suyl.designpattern.ac_proxypattern.jdk_staticproxy;

/**
 * 王二狗诉讼
 */
public class SecondDongWang implements ILawSuit {
    @Override
    public void submit(String proof) {
        System.out.println(String.format("老板欠薪跑路，证据如山：%s", proof));
    }

    @Override
    public void defend() {
        System.out.println(String.format("铁证如山，%s还钱！！！", "张三"));
    }
}
