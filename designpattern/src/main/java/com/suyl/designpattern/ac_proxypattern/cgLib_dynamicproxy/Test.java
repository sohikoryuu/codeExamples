package com.suyl.designpattern.ac_proxypattern.cgLib_dynamicproxy;

/**
 * 可见，通过cgLib对没有实现任何接口的类做了动态代理，达到了和前面一样的效果。
 * 这里只是简单的讲解了一些cgLib的使用方式，有兴趣的可以进一步了解其比较高级的功能，
 * 例如回调过滤器（CallbackFilter）等。
 * <p>
 * cgLib的动态代理原理
 * CGLIB原理：动态生成一个要代理类的子类，子类重写要代理的类的所有不是final的方法。
 * 在子类中采用方法拦截的技术拦截所有父类方法的调用，顺势织入横切逻辑。它比使用java反射的JDK动态代理要快。
 * CGLIB底层：使用字节码处理框架ASM，来转换字节码并生成新的类。不鼓励直接使用ASM，
 * 因为它要求你必须对JVM内部结构包括class文件的格式和指令集都很熟悉。
 * CGLIB缺点：对于final方法，无法进行代理。
 */
public class Test {

    public static void main(String[] args) {
        Frank cProxy = (Frank) ProxyFactory.getGcLibDynProxy(new Frank());
        cProxy.submit("工资流水在此");
        cProxy.defend();
    }
}
