package com.suyl.designpattern.ac_proxypattern.jdk_staticproxy;

/**
 * 代理律师诉讼类
 */
public class ProxyLawyer implements ILawSuit {
    ILawSuit iLawSuit;

    public ProxyLawyer(ILawSuit iLawSuit) {
        this.iLawSuit = iLawSuit;
    }

    @Override
    public void submit(String proof) {
        iLawSuit.submit(proof);
    }

    @Override
    public void defend() {
        iLawSuit.defend();
    }
}
