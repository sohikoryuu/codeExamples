package com.suyl.designpattern.ac_proxypattern.jdk_staticproxy;

/**
 * 定义一个诉讼接口
 */
public interface ILawSuit {
    void submit(String proof); // 提起诉讼

    void defend(); // 法庭辩护
}
