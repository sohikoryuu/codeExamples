package com.suyl.designpattern.ab_strategypattern.eg;

public interface DeviceStatusStrategy {

    /**
     * 处理状态接口
     *
     * @param message
     * @param deviceInfo
     * @param data
     * @return
     */
    public boolean processingStatus(String message, String deviceInfo, byte[] data);
}
