package com.suyl.designpattern.ab_strategypattern;

import com.suyl.designpattern.ab_strategypattern.service.impl.Car;
import com.suyl.designpattern.ab_strategypattern.strategy.BussCar;
import com.suyl.designpattern.ab_strategypattern.strategy.Person;
import com.suyl.designpattern.ab_strategypattern.strategy.SmallCar;

public class Test {

    public static void main(String[] args) {
        Car smallCar = new SmallCar("路虎", "黑色");
        Car bussCar = new BussCar("公交车", "白色");
        Person p1 = new Person("小明", 20);
        p1.driver(smallCar);
        p1.driver(bussCar);
    }
}
