package com.suyl.designpattern.ab_strategypattern.strategy;

import com.suyl.designpattern.ab_strategypattern.service.impl.Car;

/**
 * 具体策略实现子类
 */
public class SmallCar extends Car {
    public SmallCar(String name, String color) {
        super(name, color);
    }

    public void run() {
        System.out.println(color + " " + name + "SmallCar...");
    }
}
