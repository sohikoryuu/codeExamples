package com.suyl.designpattern.ab_strategypattern.eg;

public class saveDeviceState1500 implements DeviceStatusStrategy {
    @Override
    public boolean processingStatus(String message, String deviceInfo, byte[] data) {
        System.out.println("1500...........");
        return true;
    }
}
