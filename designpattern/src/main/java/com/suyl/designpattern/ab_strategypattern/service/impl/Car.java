package com.suyl.designpattern.ab_strategypattern.service.impl;

import com.suyl.designpattern.ab_strategypattern.service.CarFunction;

/**
 * 具体策略父类
 */
public class Car implements CarFunction {

    protected String name;
    protected String color;

    public Car(String name, String color) {
        this.name = name;
        this.color = color;
    }

    @Override
    public void run() {
        System.out.println(color + " " + name + "正在行驶...");
    }
}
