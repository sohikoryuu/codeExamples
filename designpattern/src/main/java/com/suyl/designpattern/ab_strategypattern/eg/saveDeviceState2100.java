package com.suyl.designpattern.ab_strategypattern.eg;

public class saveDeviceState2100 implements DeviceStatusStrategy {
    @Override
    public boolean processingStatus(String message, String deviceInfo, byte[] data) {
        System.out.println("2100...........");
        return true;
    }
}
