package com.suyl.designpattern.ab_strategypattern.service;

/**
 * 抽象策略类
 */
public interface CarFunction {
    void run();
}
