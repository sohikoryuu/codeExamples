package com.suyl.designpattern.ab_strategypattern.eg;

/**
 * 策略模式相对来说，还是很实用的，一般我们会碰到各种的if...else，或者switch..case，
 * 写这个时候一般情况下可以会写很多层的if..else,或者switch..case。这样写法能实现基本功能，
 * 但是如果碰到需要添加就很痛苦了，
 * <p>
 * 这种情况下，如果要添加业务，就得继续添加switch..case，非常不灵活。所以我们通过策略模式来实现对switch..case的处理。
 */
public class SwitchCase {

    public void judgmentType(String type) {
        switch (type) {
            case "1500":
                System.out.println("");
                break;
            case "1888":
                System.out.println("");
                break;
            case "2100":
                System.out.println("");
                break;
            case "5100":
                System.out.println("");
                break;
            case "2200":
                System.out.println("");
                break;
            case "2280":
                System.out.println("");
                break;
            default:
                break;
        }
    }
}
