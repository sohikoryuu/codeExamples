package com.suyl.designpattern.ab_strategypattern.strategy;

import com.suyl.designpattern.ab_strategypattern.service.impl.Car;

/**
 * 应用场景
 */
public class Person {
    private String name;    //姓名
    private Integer age;    //年龄
    private Car car;        //拥有车

    public void driver(Car car) {
        this.car = car;
        System.out.println(name + " " + age + " 岁开着");
        car.run();
    }

    public Person(String name, Integer age) {
        this.name = name;
        this.age = age;
    }
}
