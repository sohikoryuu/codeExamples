package com.suyl.designpattern.aa_singletonpattern;

/**
 * 饿汉
 * <p>
 * 这种方式基于classloder机制避免了多线程的同步问题，不过，instance在类装载时就实例化，
 * 虽然导致类装载的原因有很多种，在单例模式中大多数都是调用getInstance方法， 但是也不能确
 * 定有其他的方式（或者其他的静态方法）导致类装载，这时候初始化instance显然没有达到lazy loading的效果。
 * <p>
 * 以空间换时间，故不存在线程安全问题。
 */
public class ac_Singleton {

    private static ac_Singleton instance = new ac_Singleton();

    private ac_Singleton() {
    }

    public static ac_Singleton getInstance() {
        return instance;
    }
}
