package com.suyl.designpattern.aa_singletonpattern;

/**
 * 懒汉，线程安全
 * <p>
 * 这种写法能够在多线程中正常工作，而且也具备lazy loading，但是，
 * 遗憾的是效率很低，99%情况下不需要同步
 */
public class ab_Singleton {

    private static ab_Singleton instance;

    private ab_Singleton() {
    }

    public static synchronized ab_Singleton getInstance() {
        if (instance == null) {
            instance = new ab_Singleton();
        }
        return instance;
    }
}
