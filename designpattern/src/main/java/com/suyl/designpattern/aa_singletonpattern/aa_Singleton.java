package com.suyl.designpattern.aa_singletonpattern;

/**
 * 懒汉模式，线程不安全
 * <p>
 * 这种模式实现lazy loading很明显，但致命的是在多线程中不能正常工作
 * <p>
 * 以时间换空间，在多线程环境下存在风险。
 */
public class aa_Singleton {

    private static aa_Singleton instance;

    private aa_Singleton() {
    }

    public static aa_Singleton getInstance() {
        if (instance == null) {
            instance = new aa_Singleton();
        }
        return instance;
    }
}
