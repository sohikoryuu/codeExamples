package com.suyl.designpattern.aa_singletonpattern;

/**
 * 枚举
 * <p>
 * 这种方式是Effective Java作者Josh Bloch 提倡的方式，它不仅能避免多线程同步问题，
 * 而且还能防止反序列化重新创建新的对象，可谓是很坚强的壁垒啊，不过，个人认为由于1.5中
 * 才加入enum特性，用这种方式写不免让人感觉生疏，在实际工作中，我也很少看见有人这么写过。
 * <p>
 * 优点:线程安全、不会因为序列化而产生新实例、防止反射攻击
 */
public enum af_Singleton {

    INSTANCE {
        @Override
        protected void read() {
            System.out.println("read()");
        }

        @Override
        protected void write() {
            System.out.println("write()");
        }
    };

    protected abstract void read();

    protected abstract void write();
}
