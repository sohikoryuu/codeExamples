package com.suyl.designpattern.aa_singletonpattern;

/**
 * 饿汉,变种
 * <p>
 * 表面上看起来差别挺大，其实更第三种方式差不多，都是在类初始化即实例化instance
 * <p>
 *
 */
public class ad_Singleton {

    private static ad_Singleton instance = null;

    static {
        instance = new ad_Singleton();
    }

    private ad_Singleton() {
    }

    public static ad_Singleton getInstance() {
        return instance;
    }
}
