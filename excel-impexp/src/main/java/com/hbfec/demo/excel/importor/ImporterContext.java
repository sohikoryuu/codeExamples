package com.hbfec.demo.excel.importor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 自动添加所有的ExcelImporter
 *
 */
@Component
public class ImporterContext {

    private Map<String, AbstractExcelImporter> importerMap;

    @Autowired
    public ImporterContext(Map<String, AbstractExcelImporter> map) {
        importerMap = map;
    }

    public AbstractExcelImporter getImporterByType(String type) {
        if (importerMap != null) {
            return importerMap.get(type);
        }
        return null;
    }


}
