package com.hbfec.demo.excel.entity;

import com.hbfec.demo.excel.excelconfig.converter.CustomizeFieldReadConverter;
import com.hbfec.demo.excel.excelconfig.converter.CustomizeFieldWriteConverter;
import com.hbfec.demo.excel.excelconfig.validator.UserEmailValidator;
import com.hbfec.demo.excel.excelconfig.validator.UsernameValidator;
import com.wuwenze.poi.annotation.Excel;
import com.wuwenze.poi.annotation.ExcelField;

import java.util.Date;

@Excel
public class Tabel1 {

    @ExcelField(value = "编号", width = 30)
    private Integer userId;

    @ExcelField(//
            value = "用户名",//
            required = true,//
            validator = UsernameValidator.class,//
            comment = "请填写用户名，最大长度为12，且不能重复"
    )
    private String username;

    @ExcelField(value = "密码", required = true, maxLength = 32)
    private String password;

    @ExcelField(value = "邮箱", validator = UserEmailValidator.class)
    private String email;

    @ExcelField(//
            value = "性别",//
            readConverterExp = "未知=0,男=1,女=2"
    )
    private Integer sex;


    @ExcelField(value = "创建时间", dateFormat = "yyyy/MM/dd HH:mm:ss")
    private Date createAt;

    @ExcelField(//
            value = "自定义字段",//
            maxLength = 80,//
            comment = "可以乱填，但是长度不能超过80，导入时最终会转换为数字",//
            writeConverter = CustomizeFieldWriteConverter.class,// 写文件时，将数字转字符串
            readConverter = CustomizeFieldReadConverter.class// 读文件时，将字符串转数字
    )
    private Integer customizeField;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Integer getCustomizeField() {
        return customizeField;
    }

    public void setCustomizeField(Integer customizeField) {
        this.customizeField = customizeField;
    }
}
