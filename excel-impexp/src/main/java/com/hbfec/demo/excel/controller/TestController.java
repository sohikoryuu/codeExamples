package com.hbfec.demo.excel.controller;

import com.google.common.collect.Lists;
import com.hbfec.demo.excel.entity.Tabel1;
import com.wuwenze.poi.ExcelKit;
import com.wuwenze.poi.handler.ExcelReadHandler;
import com.wuwenze.poi.pojo.ExcelErrorField;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created with IntelliJ IDEA.
 *
 * @version v1.0
 * @author: suyl
 * @date: 2020/3/17 16:52
 * @Description: No Description
 */
@RestController
public class TestController {
    /**
     * 导入excel文件
     */
    @PostMapping(value = "ReadExcel")
    @ResponseBody
    private Object ReadExcel(HttpServletRequest request, HttpServletResponse response, @RequestParam("file") MultipartFile files) throws IOException {
        if (!files.isEmpty()) {
            InputStream inputStream = null;
            try {
                Map<String, Object> results = new HashedMap();
                List<Tabel1> successList = Lists.newArrayList();
                List<Map<String, Object>> errorList = Lists.newArrayList();
                inputStream = files.getInputStream();
                ExcelKit.$Import(Tabel1.class).readXlsx(inputStream, new ExcelReadHandler<Tabel1>() {

                    @Override
                    public void onSuccess(int sheetIndex, int rowIndex, Tabel1 entity) {
                        successList.add(entity); // 单行读取成功，加入入库队列。
                    }

                    @Override
                    public void onError(int sheetIndex, int rowIndex,
                                        List<ExcelErrorField> errorFields) {
                        // 读取数据失败，记录了当前行所有失败的数据
                        errorList.add(com.hbfec.demo.excel.excelconfig.MapUtils.newHashMap(//
                                "sheetIndex", sheetIndex,//
                                "rowIndex", rowIndex,//
                                "errorFields", errorFields.toString()//
                        ));
                    }
                });
                new ConcurrentHashMap<String, String>();
                if (errorList.size() > 0) {
                    return errorList.toString();
                } else {
                    System.out.println(successList.get(0).getCreateAt());
                    System.out.println(successList.get(0).getCustomizeField());
                    System.out.println(successList.get(0).getEmail());
                    System.out.println(successList.get(0).getPassword());
//                    autoBlackWhiteService.insertBatch(successList);
                }
            } catch (Exception e) {
                return "发生异常请联系管理员";
            } finally {
                inputStream.close();
            }
        }
        return "success";
    }

}
