package com.hbfec.demo.excel;

import com.hbfec.demo.excel.importor.AbstractExcelImporter;
import com.hbfec.demo.excel.importor.ImporterContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * Service实现
 *
 *
 */
@Service
public class ExcelServiceImpl implements ExcelService {

    private ImporterContext importerContext;


    @Autowired
    public ExcelServiceImpl(ImporterContext importerContext) {
        this.importerContext = importerContext;
    }

    @Override
    public int importExcel(String type, MultipartFile file) {

        AbstractExcelImporter excelImporter = importerContext.getImporterByType(type);

        try {
            excelImporter.processExcel(file.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }


        return excelImporter.getSuccessList().size();
    }
}