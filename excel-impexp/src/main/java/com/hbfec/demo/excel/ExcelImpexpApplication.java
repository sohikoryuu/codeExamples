package com.hbfec.demo.excel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExcelImpexpApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExcelImpexpApplication.class, args);
    }

}
