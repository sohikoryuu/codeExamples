package com.hbfec.demo.excel.excelconfig;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 *
 * @version v1.0
 * @author: suyl
 * @date: 2020/3/16 15:10
 * @Description: No Description
 */
public class MapUtils {

    public static Map<String, Object> newHashMap(String sheetIndex, int sIndex, String rowIndex, int rIndex, String errorFields, String error) {
        Map<String, Object> result = new HashMap();
        result.put(sheetIndex, sIndex);
        result.put(rowIndex, rIndex);
        result.put(errorFields, error);
        return result;
    }

    public static Map<String, Object> success() {
        Map<String, Object> result = new HashMap();
        result.put("result", "success");
        return result;
    }

    public static Map<String, Object> error(Object msg) {
        Map<String, Object> result = new HashMap();
        result.put("result", "error");
        result.put("msg", msg);
        return result;
    }

    public static Map<String, Object> success(Object data) {
        return (Map<String, Object>) success().put("data", data);
    }

}
