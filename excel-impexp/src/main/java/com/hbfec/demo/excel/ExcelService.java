package com.hbfec.demo.excel;


import org.springframework.web.multipart.MultipartFile;

/**
 * service定义
 *
 * @author makejava
 * @since 2020-03-14 15:49:01
 */
public interface ExcelService {

    int importExcel(String type, MultipartFile file);

}