package com.hbfec.demo.excel.importor;



import com.hbfec.demo.excel.entity.Tabel2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("Table2")
public class Table2Importer extends AbstractExcelImporter<Tabel2> {

    Logger logger = LoggerFactory.getLogger(Table2Importer.class);

    @Override
    Class<Tabel2> getEntityType() {
        return Tabel2.class;
    }

    @Override
    public void handleSuccessList(List<Tabel2> successList) {
        logger.debug("这个bean可再注入mapper，调用mapper保存处理成功的记录, size:{}", successList.size());
    }
}
