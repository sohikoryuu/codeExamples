package com.hbfec.demo.excel.importor;

import com.hbfec.demo.excel.entity.Tabel1;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("Table1")
public class Table1Importer extends AbstractExcelImporter<Tabel1> {

    Logger logger = LoggerFactory.getLogger(Table1Importer.class);

    @Override
    Class<Tabel1> getEntityType() {
        return Tabel1.class;
    }

    @Override
    public void handleSuccessList(List<Tabel1> successList) {
        logger.debug("这个bean可再注入mapper，调用mapper保存处理成功的记录, size:{}", successList.size());
    }
}
