package com.hbfec.demo.excel;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * 字典表(IamDict)表控制层
 *
 * @author makejava
 * @since 2020-03-14 15:49:03
 */
@RestController
@RequestMapping("excel")
public class ExcelController {
    /**
     * 服务对象
     */
    @Resource
    private ExcelService excelService;


    @PostMapping("importExcel/{type}")
    public Object importExcel(@PathVariable String type, @RequestParam MultipartFile file) {

        return excelService.importExcel(type, file);
    }



}