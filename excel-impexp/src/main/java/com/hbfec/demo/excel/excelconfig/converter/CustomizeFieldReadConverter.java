package com.hbfec.demo.excel.excelconfig.converter;

import com.wuwenze.poi.convert.ReadConverter;
import com.wuwenze.poi.exception.ExcelKitReadConverterException;

public class CustomizeFieldReadConverter implements ReadConverter {
    @Override
    public Object convert(Object o) throws ExcelKitReadConverterException {
        String value = (String) o;

        int convertedValue = 0;
        for (char c : value.toCharArray()) {
            convertedValue += Integer.valueOf(c);
        }
        return convertedValue;
    }
}
