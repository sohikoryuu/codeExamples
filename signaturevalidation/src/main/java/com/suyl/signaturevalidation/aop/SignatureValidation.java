package com.suyl.signaturevalidation.aop;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 自定义注解【签名认证】
 */
@Retention(value = RetentionPolicy.RUNTIME)
public @interface SignatureValidation {
}
