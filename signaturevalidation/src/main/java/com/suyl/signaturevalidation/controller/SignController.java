package com.suyl.signaturevalidation.controller;

import com.suyl.signaturevalidation.aop.SignatureValidation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试
 */
@RestController
public class SignController {

    @SignatureValidation
    @GetMapping("sign")
    public String sign() {
        return "sign";
    }

    @GetMapping("signv")
    public String signValidation() {
        return "signValidation";
    }
}
