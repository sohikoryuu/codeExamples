package com.suyl.signaturevalidation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SignaturevalidationApplication {

	public static void main(String[] args) {
		SpringApplication.run(SignaturevalidationApplication.class, args);
	}

}
