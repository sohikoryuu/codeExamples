package com.suyl.demo;

import freemarker.template.Configuration;
import freemarker.template.Template;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import sun.misc.BASE64Encoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
@RestController
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @GetMapping("down")
    public void down(HttpServletRequest request,
                     HttpServletResponse response) {
        String path = System.getProperty("user.dir") + "\\file\\";
        try {
            // java生成word简历
            Map<String, String> dataMap = new HashMap<String, String>();
            dataMap.put("name", "candy");
            dataMap.put("birth", "2020.02.14");
            dataMap.put("nation", "汉族");
            dataMap.put("age", "26");
            dataMap.put("iphone", "15102345612");
            dataMap.put("graduation", "清华大学");
            dataMap.put("mailbox", "candy@qq.com");
            dataMap.put("education", "博士");
            dataMap.put("startyear", "2020");
            dataMap.put("startmonth", "09");
            dataMap.put("endyear", "2020");
            dataMap.put("endmonth", "06");
            dataMap.put("school", "清华大学");
            dataMap.put("major", "软件工程");
            dataMap.put("majorcurriculum", "C语言，数据结构，线性代数，Java编程，Hadoop，软件工程，JavaScript，PS，Sqlserver，Oracle，MySQL，SpringMVC，html5，Linux等专业课");
            dataMap.put("pic", getImageStr(path + "avtor.jpg"));
            Configuration configuration = new Configuration();
            configuration.setDefaultEncoding("utf-8");
            //指定模板路径的第二种方式, 还有其他方式
            configuration.setDirectoryForTemplateLoading(new File(path));

            /**
             * 下载文件方式
             * 1.下载到本地
             * 2.网络路径下载
             */
            // 1.下载到本地
            // 输出文档路径及名称
            File outFile = new File(path + "test.doc");
            //以utf-8的编码读取ftl文件
            Template t = configuration.getTemplate("test.ftl", "utf-8");
            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile), "utf-8"), 10240);
//            t.process(dataMap, out);
            // 2.网络路径下载
            response.setCharacterEncoding("utf-8");
            response.setHeader("Content-Disposition", "attachement;filename=test1.doc");
            t.process(dataMap, response.getWriter());
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 获得图片的Base64编码
     *
     * @param imgFile
     * @return
     * @Author candy 2020-02-14 下午10:15:10
     */
    public static String getImageStr(String imgFile) {
        InputStream in = null;
        byte[] data = null;
        try {
            in = new FileInputStream(imgFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            data = new byte[in.available()];
            //注：FileInputStream.available()方法可以从输入流中阻断由下一个方法调用这个输入流中读取的剩余字节数
            in.read(data);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        BASE64Encoder encoder = new BASE64Encoder();
        return encoder.encode(data);

    }
}
