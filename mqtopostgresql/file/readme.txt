##################################################################
###             读取ActiveMQ，存到postgresql库                   ###
##################################################################
使用的技术：
1.fastjson
2.Spring Data JPA
3.postgresql
4.activemq
5.lombok
6.devtools

###########################下载#####################################
ActiveMQ 下载
http://activemq.apache.org/download-archives.html
postgresql 下载
https://www.enterprisedb.com/downloads/postgres-postgresql-downloads

###########################连接#####################################
ActiveMQ 连接
https://www.cnblogs.com/ruiati/p/8984303.html
postgresql 连接
https://blog.csdn.net/u014054441/article/details/90606516

###########################访问#####################################
ActiveMQ访问
http://localhost:8161

###########################配置#####################################
https://www.cnblogs.com/elvinle/p/8457596.html   activeMQ配置
