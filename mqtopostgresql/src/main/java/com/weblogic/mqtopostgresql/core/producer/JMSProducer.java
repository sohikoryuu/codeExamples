package com.weblogic.mqtopostgresql.core.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import javax.jms.Destination;
import javax.jms.Queue;
import javax.jms.Topic;

/**
 * 生产者
 */
@Component
public class JMSProducer {

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private JmsMessagingTemplate jms;

    @Autowired
    private Queue queue;

    @Autowired
    private Topic topic;

    // 自定义
    public void sendMessage(Destination destination, String message) {
        this.jmsTemplate.convertAndSend(destination, message);
    }

    // 默认queue
    public String queue(String message) {
        jms.convertAndSend(queue, message);
        return "queue 发送成功";
    }

    // 默认topic
    public String topic(String message) {
        jms.convertAndSend(topic, message);
        return "topic 发送成功";
    }
}
