package com.weblogic.mqtopostgresql.core.consumer;

import com.alibaba.fastjson.JSONObject;
import com.weblogic.mqtopostgresql.bussiness.entity.Book;
import com.weblogic.mqtopostgresql.bussiness.service.BookService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;


/**
 * 消费者
 */
@Component
public class JMSConsumer {

    private final static Logger logger = LoggerFactory.getLogger(JMSConsumer.class);

    @Autowired
    BookService bookService;

    // 监听queue
    @JmsListener(destination = "springboot.queue.test", containerFactory = "jmsListenerContainerQueue")
    @SendTo("out.queue")
    public String receiveQueue(String msg) {
        logger.info("接收到消息：{}", msg);
        Book book = JSONObject.parseObject(msg, Book.class);
        bookService.add(book);
        logger.info("监听到数据，解析入库【postgresql】成功");
        return JSONObject.toJSONString(book);
    }

    // 监听topic
    @JmsListener(destination = "springboot.topic.test", containerFactory = "jmsListenerContainerTopic")
    public void receive(String text) {
        System.out.println("TopicListener: consumer-a 收到一条信息: " + text);
    }
}
