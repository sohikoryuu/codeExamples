package com.weblogic.mqtopostgresql.bussiness.service;

import com.weblogic.mqtopostgresql.bussiness.entity.Book;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface BookService {

    List<Book> getBooksByAuthorStartingWith(String author);

    List<Book> getBooksByPriceGreaterThan(Double price);

    Book add(Book book);

    List<Book> getBookByIdAndAuthor(String author, Integer id);

    List<Book> getBooksByIdAndName(String name, Integer id);
}
