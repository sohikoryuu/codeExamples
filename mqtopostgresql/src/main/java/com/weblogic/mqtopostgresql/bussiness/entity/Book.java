package com.weblogic.mqtopostgresql.bussiness.entity;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity(name = "t_book")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "book_name", nullable = false)
    private String name;
    @Column(name = "author", nullable = false)
    private String author;
    @Column(name = "price", nullable = false)
    private Double price;
    @Transient
    private String description;
}
