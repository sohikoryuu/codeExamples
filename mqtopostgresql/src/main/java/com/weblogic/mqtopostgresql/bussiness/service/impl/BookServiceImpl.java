package com.weblogic.mqtopostgresql.bussiness.service.impl;

import com.weblogic.mqtopostgresql.bussiness.dao.BookRepository;
import com.weblogic.mqtopostgresql.bussiness.entity.Book;
import com.weblogic.mqtopostgresql.bussiness.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {

    @Autowired
    BookRepository bookRepository;

    @Override
    public List<Book> getBooksByAuthorStartingWith(String author) {
        return bookRepository.getBooksByAuthorStartingWith(author);
    }

    @Override
    public List<Book> getBooksByPriceGreaterThan(Double price) {
        return bookRepository.getBooksByPriceGreaterThan(price);
    }

    @Override
    public Book add(Book book) {
        return bookRepository.save(book);
    }

    @Override
    public List<Book> getBookByIdAndAuthor(String author, Integer id) {
        return bookRepository.getBookByIdAndAuthor(author, id);
    }

    @Override
    public List<Book> getBooksByIdAndName(String name, Integer id) {
        return bookRepository.getBooksByIdAndName(name, id);
    }
}
