package com.weblogic.mqtopostgresql.bussiness.controller;

import com.alibaba.fastjson.JSONObject;
import com.weblogic.mqtopostgresql.bussiness.entity.Book;
import com.weblogic.mqtopostgresql.core.producer.JMSProducer;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.jms.Destination;

/**
 * @author suyanlong
 * @version 1.0
 * @createDate 2019/12/6 23:52
 * @Description 测试ActiveMQ消息队列存取
 */
@RestController
@RequestMapping("producer")
public class MqController {

    @Autowired
    JMSProducer jmsProducer;

    @GetMapping("queue")
    public String queue() {
        // 自定义mq - name
        Destination queue = new ActiveMQQueue("springboot.queue.test");
        Book book = new Book();
        book.setAuthor("苏彦龙");
        book.setDescription("这个工程师读取ActiveMQ的信息，存储到postgresql数据库");
        book.setName("mqtopostgresql");
        book.setPrice(23.54);
        jmsProducer.sendMessage(queue, JSONObject.toJSONString(book));
        // 默认 mq - name
        jmsProducer.queue(JSONObject.toJSONString(book));
        return "bookTOmqSuccess";
    }

    @GetMapping("topic")
    public String topic() {
        // 自定义 mq - topic
        Destination topic = new ActiveMQTopic("springboot.topic.test");
        Book book = new Book();
        book.setAuthor("苏彦龙");
        book.setDescription("这个工程师读取ActiveMQ的信息，存储到postgresql数据库");
        book.setName("mqtopostgresql");
        book.setPrice(23.54);
        jmsProducer.sendMessage(topic, JSONObject.toJSONString(book));
        // 默认 mq - name
        return jmsProducer.topic(JSONObject.toJSONString(book));
    }
}
