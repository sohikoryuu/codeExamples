package com.weblogic.mqtopostgresql.bussiness.controller;

import com.weblogic.mqtopostgresql.bussiness.entity.Book;
import com.weblogic.mqtopostgresql.bussiness.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author suyanlong
 * @version 1.0
 * @createDate 2019/12/7 11:28
 * @Description controller测试 postgresql数据库存取
 */
@RestController
public class PostgresqlController {

    @Autowired
    BookService bookService;

    @GetMapping("add")
    @ResponseBody
    public Object add() {
        Book book = new Book();
        book.setAuthor("苏彦龙");
        book.setDescription("这个工程师读取ActiveMQ的信息，存储到postgresql数据库");
        book.setName("mqtopostgresql");
        book.setPrice(23.54);
        return bookService.add(book);
    }

    @GetMapping("find")
    @ResponseBody
    public Object find() {
        return bookService.getBooksByAuthorStartingWith("苏彦龙");
    }
}
