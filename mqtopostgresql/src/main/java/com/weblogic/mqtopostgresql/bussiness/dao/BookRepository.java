package com.weblogic.mqtopostgresql.bussiness.dao;

import com.weblogic.mqtopostgresql.bussiness.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<Book, Integer> {

    List<Book> getBooksByAuthorStartingWith(String author);

    List<Book> getBooksByPriceGreaterThan(Double price);

    @Query(value = "select * from t_book  where id > :id and author = :author", nativeQuery = true)
    List<Book> getBookByIdAndAuthor(@Param("author") String author, @Param("id") Integer id);

    @Query("select b from t_book b where b.id < ?2 and b.name like %?1%")
    List<Book> getBooksByIdAndName(String name, Integer id);
}
