package com.weblogic.mqtopostgresql;

import lombok.extern.log4j.Log4j2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;

@SpringBootApplication
@EnableJms
@Log4j2
public class MqtopostgresqlApplication {

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        SpringApplication.run(MqtopostgresqlApplication.class, args);
        log.info("【MqtopostgresqlApplication】 start-up success...");
        log.info("start-up use time:" + (System.currentTimeMillis() - start) + "ms");
    }

}
