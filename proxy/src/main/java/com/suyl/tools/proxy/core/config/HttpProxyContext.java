package com.suyl.tools.proxy.core.config;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author suyanlong
 * @Date 2020/6/14 16:45
 * @Version 1.0
 * @Description 存储代理地址
 */
public class HttpProxyContext {
    private final static Map<String, String> map = new ConcurrentHashMap<String, String>(16);

    public static void add(String url, String proxyUrl) {
        map.put(url, proxyUrl);
    }

    public static boolean containsKey(String url) {
        return map.containsKey(url);
    }

    public static String get(String url) {
        return map.get(url);
    }
}
