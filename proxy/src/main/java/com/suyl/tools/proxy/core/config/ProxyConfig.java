package com.suyl.tools.proxy.core.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @Author suyanlong
 * @Date 2020/6/14 17:55
 * @Version 1.0
 * @Description 代理配置文件
 */
@Component
public class ProxyConfig {
    @Value("${proxy.ip}")
    private String ip;

    @Value("${proxy.port}")
    private Integer port;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }
}
