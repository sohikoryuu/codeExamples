package com.yvan.dev2word;

import lombok.Data;
import org.springframework.util.StringUtils;

import java.io.Serializable;


/**
 * @author yvan
 * @version 1.0.0
 * @description 消息响应体
 * @date 2019-11-02 14:00:00
 */
@Data
public class ResponseMsg<T> implements Serializable {
    private static final long serialVersionUID = -3481261323376560666L;

    /**
     * 记录集总页数
     */
    protected Long totalCount;

    /**
     * 实体对象或对象集
     */
    protected T data;

    /**
     * 成功标记
     */
    protected Boolean success;

    /**
     * json代码
     */
    protected Long code;

    /**
     * json信息
     */
    protected String message;
    /**
     *
     */
    protected long lastTime;
    /**
     *
     */
    protected String url;

    /**
     * 默认成功消息
     *
     * @author Wu.Liang
     */
    public ResponseMsg() {
        this.success = true;
        this.message = "操作成功";
        this.lastTime = System.currentTimeMillis();
    }

    /**
     * 成功消息 + 实体
     *
     * @param data 数据
     * @author Wu.Liang
     */
    public ResponseMsg(T data) {
        this.success = true;
        this.message = "操作成功";
        this.lastTime = System.currentTimeMillis();
        this.data = data;
    }

    public ResponseMsg(T data, Long totalCount) {
        this.success = true;
        this.totalCount = totalCount;
        this.message = "操作成功";
        this.lastTime = System.currentTimeMillis();
        this.data = data;
    }

    /**
     * 生成错误消息
     *
     * @param errorMsg 错误提示消息，默认“操作失败”
     * @author Wu.Liang
     */
    public ResponseMsg(String errorMsg) {
        this.success = false;
        this.message = StringUtils.isEmpty(errorMsg) ? "操作失败" : errorMsg;
        this.lastTime = System.currentTimeMillis();
    }

    /**
     * 生成消息
     *
     * @author Wu.Liang
     */
    public ResponseMsg(boolean success, String msg) {
        this.success = success;
        this.message = StringUtils.isEmpty(msg) ? "操作失败" : msg;
        this.lastTime = System.currentTimeMillis();
    }
}
