package com.yvan.dev2word.db2word.utils;

import org.apache.velocity.Template;
import org.apache.velocity.app.Velocity;

import java.util.Properties;

public class VelocityUtil {
    static{
        Properties p = new Properties();
        // 设置输入输出编码类型。和这次说的解决的问题无关
        p.setProperty(Velocity.INPUT_ENCODING, "UTF-8");
        p.setProperty(Velocity.OUTPUT_ENCODING, "UTF-8");
        //文件缓存
        p.setProperty(Velocity.FILE_RESOURCE_LOADER_CACHE, "false");
        // 这里加载类路径里的模板而不是文件系统路径里的模板
        p.setProperty("file.resource.loader.class",
                "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        Velocity.init(p);
    }

    public static Template getTemplateInstance(String templatePath){
        return Velocity.getTemplate(templatePath);
    }

}
