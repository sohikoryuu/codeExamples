package com.yvan.dev2word.swagger2doc.service;

import com.yvan.dev2word.db2word.vo.Table;
import com.yvan.dev2word.db2word.vo.TableDesc;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.util.List;

public interface TableService {
    List<Table> findAllTable(DriverManagerDataSource dataSource);

    List<TableDesc> findByTableName(String tableName, DriverManagerDataSource dataSource);

    DriverManagerDataSource createDataSource(String jdbcUrl, String jdbcUsername, String jdbcPassword);
}
