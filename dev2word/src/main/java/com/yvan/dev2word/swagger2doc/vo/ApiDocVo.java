package com.yvan.dev2word.swagger2doc.vo;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
public class ApiDocVo {
    /**
     * 访问地址
     */
    private String apiName;
    /**
     * 访问地址
     */
    private String baseUrl;
    /**
     * 请求哏路径
     */
    private String rootPath;

    /**
     * 所有接口
     */
    private List<ApiInterfaceVo> interfaceVoList = new ArrayList<>();
    /**
     * 分类接口
     */
    private Map<String, List<ApiInterfaceVo>> map;
}
