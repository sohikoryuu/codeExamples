package com.suyl.pickinfo.utils;

import com.suyl.pickinfo.constant.Constant;
import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import org.springframework.util.StringUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

/**
 * @author ：suyanlong
 * @date ：Created in 2020/8/29 16:44
 * @description：获取图片中的信息
 * @version: 1.0
 */
public class ObtainPicInfo {

    public static String getInfo(String path) throws URISyntaxException, TesseractException, IOException {
        if (StringUtils.isEmpty(path)) {
            return Constant.ErrMsg;
        }
        // 获取文件
        File imageFile = new File(path);
        if (!imageFile.exists()) {
            return Constant.fileNo;
        }
        //创建tess对象
        ITesseract instance = new Tesseract();
        String dictPath = new File(ObtainPicInfo.class.getResource("/").toURI()).getPath() + File.separator + Constant.tessdataFolder;
        //设置训练文件目录
        instance.setDatapath(dictPath);
        //设置训练语言
        instance.setLanguage("chi_sim"); //chi_sim ：简体中文， eng	根据需求选择语言库
        // 将图片转换成流文件处理
        BufferedImage textImage = ImageIO.read(imageFile);
        //执行转换
        return instance.doOCR(textImage);
    }
}
