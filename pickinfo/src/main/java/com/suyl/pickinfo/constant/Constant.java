package com.suyl.pickinfo.constant;

/**
 * @author ：suyanlong
 * @date ：Created in 2020/8/29 16:45
 * @description：常量
 * @version: 1.0
 */
public class Constant {

    public static final String ErrMsg = "参数不能为空！！！";
    public static final String fileNo = "文件不存在！！！";
    public static final String tessdataFolder = "tessdata";
}
