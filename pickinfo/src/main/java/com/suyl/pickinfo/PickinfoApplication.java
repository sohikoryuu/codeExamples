package com.suyl.pickinfo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PickinfoApplication {

    public static void main(String[] args) {
        SpringApplication.run(PickinfoApplication.class, args);
    }

}
