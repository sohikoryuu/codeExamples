CREATE DATABASE file_server;
USE file_server;

CREATE TABLE tb_file (
  uuid       VARCHAR(32)  NOT NULL PRIMARY KEY,
  url        VARCHAR(256) NOT NULL,
  name       VARCHAR(256) NOT NULL,
  size       BIGINT       NOT NULL,
  createdate DATETIME     NOT NULL,
  expire     VARCHAR(1)   NOT NULL,
  status     VARCHAR(2)   NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_general_ci;

# 删除7天前的可过期数据以及不可过期但是文件状态不是上传成功的数据，每天凌晨1点执行一次
CREATE EVENT e_delete_file_daily ON SCHEDULE EVERY 1 DAY STARTS DATE_ADD(date_sub(CURDATE(),interval -1 day) , INTERVAL 1 HOUR) DO DELETE FROM tb_file WHERE (status!='2' or expire='1') AND TO_DAYS(NOW())-TO_DAYS(createdate)>7;
ALTER EVENT e_delete_file_daily ENABLE;