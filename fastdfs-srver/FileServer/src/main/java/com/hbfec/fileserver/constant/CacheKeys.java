package com.hbfec.fileserver.constant;

/**
 * 缓存KEY常量
 */
public final class CacheKeys {

    private CacheKeys() {
    }

    /**
     * 文件信息，采用list方式存储
     * 列表项：[uuid] | [url] | [name] | [size] | [createdate] | [expire（是否过期）] | [status（上传状态）]
     * 备注：
     * 1. 在将文件上传至FastDFS之前，传入的数据包括uuid、url（为空字符串）、name、size、createdate、expire、status（状态为UPLOADING）
     * 2. 当上传文件至FastDFS之后，传入的数据包括uuid、url、name、size、createdate、expire、status（状态为UPLOADED）
     * 3. 在删除文件时，传入的数据包括uuid、url、name、size、createdate、expire、status（状态为DELETED）
     */
    public static final String FILE_QUEUE = "fileq";
    // 文件信息的备份list
    public static final String FILE_QUEUE_BACKUP = "fileqb";
    // 分布式锁
    public static final String FILE_QUEUE_LOCK = "fileql";

    /**
     * 文件信息镜像，采用字符串的方式存储
     * 该项的目的是为了解决如下问题：当文件上传成功的信息已经插入缓存队列但还未入库时，此时客户端进行下载，会出现“文件不存在的问题”。
     * 解决思路：当将文件上传成功的信息插入缓存后，同时设置一个该文件信息的镜像写入缓存。<BR/>
     * 当客户端需要进行某些操作时，先判断缓存中是否存在该镜像，<BR/>
     * 如果存在，则循环等待，直到不存在为止，然后查询数据库确认是否真的已经入库成功；如果不存在，则直接查询数据。<BR/>
     * 持久化定时任务在将缓存中的信息入库成功后，除了要删除缓存队列中的内容，还要将这些镜像删除
     * key：CacheKeys.FILE_STATUS + [uuid] + _ + [status]
     * value：空字符串
     * 备注：
     * 1. 不允许过期
     */
    public static final String FILE_STATUS = "files_";
}
