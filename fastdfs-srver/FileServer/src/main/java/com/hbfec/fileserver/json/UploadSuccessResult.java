package com.hbfec.fileserver.json;

/**
 * 上传成功的result
 */
public class UploadSuccessResult {
    private String uuid;

    public UploadSuccessResult(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public String toString() {
        return "{\"UploadSuccessResult\":{"
                + "\"uuid\":\"" + uuid + "\""
                + "}}";
    }
}
