package com.hbfec.fileserver.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.text.SimpleDateFormat;

/**
 * Jackson 配置类
 */
@Configuration
public class JacksonConfig {
    /**
     * 获取 ObjectMapper Bean
     *
     * @param dateFormat 时间格式
     * @return ObjectMapper Bean
     */
    @Bean
    public ObjectMapper getObjectMap(@Value("${fileserver.dateformat}") String dateFormat) {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        objectMapper.setDateFormat(new SimpleDateFormat(dateFormat));
        return objectMapper;
    }
}
