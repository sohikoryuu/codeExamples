package com.hbfec.fileserver.constant;

/**
 * 参数验证常量类
 */
public final class Parameter {
    private Parameter() {
    }

    public static final int UUID_LENGTH_LIMIT = 32;
    public static final int FILE_NAME_LENGTH_LIMIT = 255;
}
