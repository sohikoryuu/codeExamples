package com.hbfec.fileserver.constant;

/**
 * TAG常量类
 */
public final class Tag {
    private Tag() {
    }

    public static final int SUCCESS = 1;
    public static final int FAILURE = 0;
}
