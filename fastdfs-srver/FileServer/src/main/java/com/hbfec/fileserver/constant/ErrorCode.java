package com.hbfec.fileserver.constant;

/**
 * 错误码常量类
 */
public final class ErrorCode {

    private ErrorCode() {
    }

    public static final int SUCCESS = 0;
    public static final int UNKNOWN_ERROR = 1;
    public static final int SERVICE_TEMPORARILY_UNAVAILABLE = 2;
    public static final int UNSUPPORTED_OPENAPI_METHOD = 3;
    public static final int OPEN_API_REQUEST_LIMIT_REACHED = 4;
    public static final int UNAUTHORIZED_CLIENT_IP_ADDRESS = 5;
    public static final int NO_PERMISSION_TO_ACCESS_DATA = 6;
    public static final int NO_PERMISSION_TO_ACCESS_DATA_FOR_THIS_REFERER = 7;
    public static final int TIME_OUT = 8;

    public static final int INVALID_PARAMETER = 100;

    public static final int ACCESS_TOKEN_INVALID_OR_NO_LONGER_VALID = 110;
    public static final int ACCESS_TOKEN_EXPIRED = 111;
    public static final int SESSION_KEY_EXPIRED = 112;

    public static final int UNKNOWN_DATA_STORE_API_ERROR = 800;
    public static final int INVALID_OPERATION = 801;
    public static final int DATA_STORE_ALLOWABLE_QUOTA_WAS_EXCEEDED = 802;
    public static final int SPECIFIED_OBJECT_CANNOT_BE_FOUND = 803;
    public static final int SPECIFIED_OBJECT_ALREADY_EXISTS = 804;
    public static final int DATABASE_ERROR = 805;

    public static final int NO_SUCH_APPLICATION_EXISTS = 900;
    public static final int SUPPORT_SERVICE_UNAVAILABLE = 901;
    public static final int ACCESS_PLUGIN_UNAVAILABLE = 902;

}
