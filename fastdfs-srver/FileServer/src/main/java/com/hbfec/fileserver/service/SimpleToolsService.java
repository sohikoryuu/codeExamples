package com.hbfec.fileserver.service;

import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * 简单工具Service
 */
@Service
public class SimpleToolsService {
    /**
     * 生成uuid
     *
     * @return uuid
     */
    public String generateUUID() {
        UUID uuid = UUID.randomUUID();
        String str = uuid.toString();
        // 去掉"-"符号
        return str.substring(0, 8) + str.substring(9, 13) + str.substring(14, 18)
                + str.substring(19, 23) + str.substring(24);
    }

    /**
     * Date类型转换为字符串
     *
     * @param date   时间Date
     * @param format 时间格式
     * @return 成功返回对象，失败返回null
     */
    public String dateToString(Date date, String format) {
        try {
            return new SimpleDateFormat(format).format(date);
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * 字符串转换为Date类型
     *
     * @param dateStr     字符串
     * @param dateFormate 时间格式
     * @return Date类型时间
     * @throws Exception Exception
     */
    public Date strToDate(String dateStr, String dateFormate) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormate);
        return formatter.parse(dateStr);
    }

    /**
     * 获取文件名的后缀
     *
     * @param fileName 文件名
     * @return 返回后缀名的toLowerCase形式，没有后缀返回null
     */
    public String getFileNameSuffix(String fileName) {
        String[] fileNameParts = fileName.split("\\.");
        return fileNameParts.length > 1 ? fileNameParts[fileNameParts.length - 1].toLowerCase() : null;
    }
}
