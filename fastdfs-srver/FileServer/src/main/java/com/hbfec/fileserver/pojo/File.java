package com.hbfec.fileserver.pojo;

import java.util.Date;

/**
 * 文件信息
 */
public class File {
    private String uuid;
    private String url;
    private String name;
    private Long size;
    private Date createDate;
    private String expire;
    private String status;

    public File() {
    }

    public File(String uuid, String url, String name, Long size, Date createDate, String expire, String status) {
        this.uuid = uuid;
        this.url = url;
        this.name = name;
        this.size = size;
        this.createDate = createDate;
        this.expire = expire;
        this.status = status;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getExpire() {
        return expire;
    }

    public void setExpire(String expire) {
        this.expire = expire;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
