package com.hbfec.fileserver.json;

/**
 * 返回结果实体类
 */
public class Response {
    private int tag;
    private int code;
    private String reason;
    private Object result;

    public Response() {
    }

    public Response(int tag, int code, String reason, Object result) {
        this.tag = tag;
        this.code = code;
        this.reason = reason;
        this.result = result;
    }

    public int getTag() {
        return tag;
    }

    public void setTag(int tag) {
        this.tag = tag;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "{\"Response\":{"
                + "\"tag\":\"" + tag + "\""
                + ", \"code\":\"" + code + "\""
                + ", \"reason\":\"" + reason + "\""
                + ", \"result\":" + result
                + "}}";
    }
}
