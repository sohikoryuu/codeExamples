package com.hbfec.fileserver.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Web 配置类
 */
@Configuration
public class WebConfig {
    /**
     * CORS配置
     *
     * @return WebMvcConfigurer
     */
    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Value("${fileserver.cors.allowed-origins}")
            private String allowedOrigins;
            @Value("${fileserver.cors.allowed-methods}")
            private String allowedMethods;

            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedMethods(allowedMethods.split(","))
                        .allowedOrigins(allowedOrigins.split(","));
            }
        };
    }
}
