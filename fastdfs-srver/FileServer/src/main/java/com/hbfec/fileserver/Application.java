package com.hbfec.fileserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * ###################### 说明文档 ######################
 * 目前处于 启动器1 状态
 * 如果打包项目切换到 启动器2 状态
 * 1.pom.xml
 *      <!--<packaging>war</packaging>--> 放开
 * 2.pom.xml
 *      <!--<scope>provided</scope>--> fang开
 * 3.Application.java
 *      屏蔽 启动器1 ，放开 启动器2
 * ###################### 说明文档 ######################
 */

/**
 * 启动器1
 */
@SpringBootApplication
@EnableScheduling
public class Application {
    /**
     * main方法
     *
     * @param args 参数
     */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
///**
// * 启动器2
// */
//@SpringBootApplication
//@EnableScheduling
//public class Application extends SpringBootServletInitializer {
//	/**
//	 * main方法
//	 *
//	 * @param args 参数
//	 */
//	public static void main(String[] args) {
//		SpringApplication.run(Application.class, args);
//	}
//
//	@Override
//	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
//		return application.sources(Application.class);
//	}
//}