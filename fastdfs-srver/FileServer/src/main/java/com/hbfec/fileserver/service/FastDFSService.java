package com.hbfec.fileserver.service;

import org.apache.commons.pool2.impl.GenericObjectPool;
import org.csource.fastdfs.DownloadCallback;
import org.csource.fastdfs.StorageClient;
import org.csource.fastdfs.TrackerServer;
import org.csource.fastdfs.UploadCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * FastDFS Service
 */
@Service
public class FastDFSService {
    private static final Logger LOGGER = LoggerFactory.getLogger(FastDFSService.class);

    @Autowired
    @Qualifier("trackerServerGenericObjectPool")
    private GenericObjectPool<TrackerServer> pool;

    /**
     * 上传文件
     *
     * @param fileSize      文件大小
     * @param fileExtension 文件后缀
     * @param callback      回调
     * @return 文件地址
     */
    public String[] uploadFile(Long fileSize, String fileExtension, UploadCallback callback) {
        TrackerServer trackerServer = null;
        try {
            try {
                trackerServer = pool.borrowObject();
            } catch (Exception ex) {
                LOGGER.error("从TrackerServer连接池中获取资源时出现异常", ex);
                return null;
            }
            StorageClient storageClient = null;
            try {
                storageClient = new StorageClient(trackerServer, null);
            } catch (Exception ex) {
                LOGGER.error("在上传文件前实例化StorageClient时出现异常", ex);
                return null;
            }
            try {
                return storageClient.upload_file(null, fileSize, callback, fileExtension, null);
            } catch (Exception ex) {
                LOGGER.error("上传文件时出现异常", ex);
                return null;
            }
        } finally {
            if (trackerServer != null) {
                pool.returnObject(trackerServer);
            }
        }
    }

    /**
     * 删除文件
     *
     * @param groupName 组名
     * @param fileName  文件名
     * @return 结果
     */
    public int deleteFile(String groupName, String fileName) {
        TrackerServer trackerServer = null;
        try {
            try {
                trackerServer = pool.borrowObject();
            } catch (Exception ex) {
                LOGGER.error("从TrackerServer连接池中获取资源时出现异常", ex);
                return -1;
            }
            StorageClient storageClient = null;
            try {
                storageClient = new StorageClient(trackerServer, null);
            } catch (Exception ex) {
                LOGGER.error("在删除文件前实例化StorageClient时出现异常", ex);
                return -1;
            }
            try {
                return storageClient.delete_file(groupName, fileName);
            } catch (Exception ex) {
                LOGGER.error("删除文件时出现异常", ex);
                return -1;
            }
        } finally {
            if (trackerServer != null) {
                pool.returnObject(trackerServer);
            }
        }
    }

    /**
     * 下载文件
     *
     * @param groupName 组名
     * @param fileName  文件名
     * @param callback  回调
     * @return 结果
     */
    public int downloadFile(String groupName, String fileName, DownloadCallback callback) {
        TrackerServer trackerServer = null;
        try {
            try {
                trackerServer = pool.borrowObject();
            } catch (Exception ex) {
                LOGGER.error("从TrackerServer连接池中获取资源时出现异常", ex);
                return -1;
            }
            StorageClient storageClient = null;
            try {
                storageClient = new StorageClient(trackerServer, null);
            } catch (Exception ex) {
                LOGGER.error("在下载文件前实例化StorageClient时出现异常", ex);
                return -1;
            }
            try {
                return storageClient.download_file(groupName, fileName, callback);
            } catch (Exception ex) {
                LOGGER.error("下载文件时出现异常", ex);
                return -1;
            }
        } finally {
            if (trackerServer != null) {
                pool.returnObject(trackerServer);
            }
        }
    }
}
