package com.hbfec.fileserver.config;

import com.hbfec.fileserver.service.TrackerServerFactory;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.csource.fastdfs.ClientGlobal;
import org.csource.fastdfs.TrackerServer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

/**
 * FastDFS 配置类
 */
@Configuration
public class FastDFSConfig {
    /**
     * TrackerServer连接池Bean
     *
     * @param charset                        charset
     * @param connectTimeout                 connectTimeout
     * @param networkTimeout                 networkTimeout
     * @param trackerServerHost              trackerServerHost
     * @param maxActive                      maxActive
     * @param maxIdle                        maxIdle
     * @param minIdle                        minIdle
     * @param maxWaitMillis                  maxWaitMillis
     * @param blockWhenExhausted             blockWhenExhausted
     * @param timeBetweenEvictionRunsMillis  timeBetweenEvictionRunsMillis
     * @param softMinEvictableIdleTimeMillis softMinEvictableIdleTimeMillis
     * @param trackerServerFactory           trackerServerFactory
     * @return Bean
     * @throws Exception Exception
     */
    @Bean(name = "trackerServerGenericObjectPool")
    public GenericObjectPool<TrackerServer> getGenericObjectPool(
            @Value("${fileserver.charset}") String charset,
            @Value("${fileserver.fastdfs.connect-timeout}") String connectTimeout,
            @Value("${fileserver.fastdfs.network-timeout}") String networkTimeout,
            @Value("${fileserver.fastdfs.tracker-server}") String trackerServerHost,
            @Value("${fileserver.fastdfs.pool.max-active}") int maxActive,
            @Value("${fileserver.fastdfs.pool.max-idle}") int maxIdle,
            @Value("${fileserver.fastdfs.pool.min-idle}") int minIdle,
            @Value("${fileserver.fastdfs.pool.max-wait-millis}") long maxWaitMillis,
            @Value("${fileserver.fastdfs.pool.block-when-exhausted}") boolean blockWhenExhausted,
            @Value("${fileserver.fastdfs.pool.time-between-eviction-runs-millis}") long timeBetweenEvictionRunsMillis,
            @Value("${fileserver.fastdfs.pool.soft-min-evictable-idle-time-millis}") long softMinEvictableIdleTimeMillis,
            TrackerServerFactory trackerServerFactory) throws Exception {
        Properties prop = new Properties();
        prop.setProperty("fastdfs.charset", charset);
        prop.setProperty("fastdfs.connect_timeout_in_seconds", connectTimeout);
        prop.setProperty("fastdfs.network_timeout_in_seconds", networkTimeout);
        prop.setProperty("fastdfs.tracker_servers", trackerServerHost);
        ClientGlobal.initByProperties(prop);
        GenericObjectPoolConfig genericObjectPoolConfig = new GenericObjectPoolConfig();
        genericObjectPoolConfig.setJmxEnabled(false);
        genericObjectPoolConfig.setMaxTotal(maxActive);
        genericObjectPoolConfig.setMaxIdle(maxIdle);
        genericObjectPoolConfig.setMinIdle(minIdle);
        genericObjectPoolConfig.setMaxWaitMillis(maxWaitMillis);
        genericObjectPoolConfig.setBlockWhenExhausted(blockWhenExhausted);
        genericObjectPoolConfig.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);
        genericObjectPoolConfig.setSoftMinEvictableIdleTimeMillis(softMinEvictableIdleTimeMillis);
        return new GenericObjectPool<TrackerServer>(trackerServerFactory, genericObjectPoolConfig);
    }
}
