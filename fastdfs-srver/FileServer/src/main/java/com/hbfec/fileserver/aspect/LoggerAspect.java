package com.hbfec.fileserver.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * Controller切面——日志
 */
@Aspect
@Component
public class LoggerAspect {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoggerAspect.class);

    /**
     * 日志 Pointcut
     */
    @Pointcut("execution(* com.hbfec.fileserver.controller.*.*(..))")
    public void log() {
    }

    /**
     * 日志切面
     *
     * @param jp JoinPoint
     */
    @Before("log()")
    public void before(JoinPoint jp) {
        LOGGER.debug(String.format("method={%s}, arg={%s}", jp.getSignature(), Arrays.asList(jp.getArgs())));
    }
}
