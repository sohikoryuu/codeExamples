package com.hbfec.fileserver.constant;

/**
 * 文件状态常量类
 */
public final class FileStatus {
    private FileStatus() {
    }

    public static final String UPLOADING = "1";
    public static final String UPLOADED = "2";
    public static final String DELETED = "3";
}
