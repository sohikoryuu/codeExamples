package com.hbfec.fileserver.callback;

import org.csource.fastdfs.UploadCallback;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * 自定义的FastDFS回调，用于读取文件上传输入流，并写入到FastDFS文件流
 */
public class CustomUploadCallback implements UploadCallback {
    private InputStream inputStream;

    public CustomUploadCallback(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    /**
     * 上传回调
     *
     * @param out OutputStream
     * @return 结果
     * @throws IOException IOException
     */
    public int send(OutputStream out) throws IOException {
        int readBytes;
        byte[] buff = new byte[1024 * 1024];
        while ((readBytes = inputStream.read(buff)) >= 0) {
            if (readBytes == 0) {
                continue;
            }
            out.write(buff, 0, readBytes);
        }
        return 0;
    }
}