package com.hbfec.fileserver.callback;

import com.hbfec.fileserver.json.Response;

import java.io.InputStream;
import java.util.HashMap;

/**
 * 上传业务的回调接口，当迭代到文件部分时，会触发handle
 */
public interface IUploadLogicCallback {
    /**
     * 处理文件上传业务
     *
     * @param argMap      表单参数
     * @param fileName    文件名
     * @param inputStream 文件流
     * @return 结果
     */
    Response handle(HashMap argMap, String fileName, InputStream inputStream);
}
