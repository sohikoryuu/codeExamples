# codeExamples代码示例

#### 介绍
存放一些代码示例,

#### 软件架构
本项目中分为几个子项目，分别存放代码示例

1.**easypoidemo**  此项目是spring boot导入excel时，验证excel中的数据项目示例 :arrow_right:  [ **JAVA读写excel文件** ](https://blog.csdn.net/weixin_39585051/article/details/105072866)

2.**qrcode** 此项目是生成条形码和二维码项目

3.**springSession** 此项目是spring Boot + redis 实现session共享方案

4.**mqtopostgresql** 将ActiveMQ数据解析入库到postgresql中，spring boot + spring Data JPA + ActiveMQ + postgresql + fastjson + lombok + devtools

5.**java_pdf_demo** java生成pdf示例

6.**callback** 异步回调示例

7.**configproject** spring Cloud示例配置中心（其他代码未上传）

8.**designpattern** 设计模式代码示例

9.**fastdfs-srver** 文件服务器中间件，安装部署说明及代码

10.**itext-sample** 实现html转pdf示例

11.**kaptcha** 验证码示例

12.**mongodb-demo** MongoDB连接及应用示例及【布隆过滤器】

13.**multdb-parent** 多数据源案例、读写分离、自定义注解

14.**rabbitmq** rabbitmq的安装，应用及常见的unacked问题

15.**signaturevalidation** 基于自定义注解的接口签名认证

16.**springboot-mail** 基于spring boot的邮件中间件，基于QQ、网易系列的邮件中心

17.**weather** 承接高并发的天气查询接口，spring boot + spring Data JPA + mysql + redis(缓存） + fastjson + lombok + devtools + jsoncode + httpclient远程连接 + jwt鉴权

18.**demo** java生成word文档

19.**cup-full** 模拟cpu飙升100%且高居不下。问题排查参见 :bowtie:[ **线上cpu使用100%高居不下原因分析** ](https://blog.csdn.net/weixin_39585051/article/details/105142019)

20.**redisbf** redis搭建布隆过滤器，且用法，爬虫获取本机ip。相关文章 [redis位图详解与应用](https://blog.csdn.net/weixin_39585051/article/details/104711566)  [布隆过滤器](https://www.jianshu.com/p/c8698e1db59f)

21.**markbfbc** 各种例子，详细说明见file/readme.txt

22.**proxy** 代理转发前台请求模板，请求在配置文件中配置

23.**dev2word** 根据数据库生成数据库文档，方便数据库变更后数据库文档维护

24.**pickinfo** java识别图片中的文字

25.**mutidatasource** mybatis-plus多数据源连接mysql

26.**annotation-demo** 自定义注解的使用，里边包含自定义注解的具体使用方法 @HandlingTime 注解使用在方法上，可以打印出某方法执行的时长；@Person 注解可以以此方式增加一些字段验证等[另一种注解解析方式参考](https://www.jianshu.com/p/c8af93d30914)

27.**delayMsg** rabbitMq搭建及延时消息消费