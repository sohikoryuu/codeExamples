package com.candy.springbootmail;

import com.candy.springbootmail.service.IMailService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
class SpringbootMailApplicationTests {

    @Autowired
    IMailService iMailService;

    @Test
    void contextLoads() {
//        iMailService.sendSimpleMail("1336805610@qq.com", "这是用代码发的邮件", "<h1>Dear,candy 哈哈</h1>");
        iMailService.sendHtmlMail("1070822910@qq.com", "这是用代码发的邮件", "<h3>Dear,candy</h3><br>This is an email sent to you in code.");
//        iMailService.sendAttachmentsMail("1336805610@qq.com", "这是用代码发的邮件", "<h1>Dear,candy 哈哈</h1>",
//                "C:\\Users\\suyl\\Desktop\\java.txt");
    }
}
