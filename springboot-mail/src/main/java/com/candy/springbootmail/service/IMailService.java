package com.candy.springbootmail.service;

import org.springframework.stereotype.Service;

@Service
public interface IMailService {

    public void sendSimpleMail(String to, String subject, String content);

    public void sendHtmlMail(String to, String subject, String content);

    public void sendAttachmentsMail(String to, String subject, String content, String filePath);
}
