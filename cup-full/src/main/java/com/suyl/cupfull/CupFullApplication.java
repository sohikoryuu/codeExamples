package com.suyl.cupfull;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class CupFullApplication {

    public static void main(String[] args) {
        SpringApplication.run(CupFullApplication.class, args);
    }

    /**
     * cpu不停的运行
     */
    @GetMapping("cpu")
    public void cpu() {
        while (true) {
        }
    }
}
