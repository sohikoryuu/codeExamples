package com.suyl.mongodbdemo.controller;

import com.suyl.mongodbdemo.bean.User;
import com.suyl.mongodbdemo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@ResponseBody
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("addUser")
    public String addUser(User user) {
        userService.InsUser(user);
        return "success";
    }

    @GetMapping("getUser")
    public Object getUser(String id, String name) {
        if (StringUtils.isEmpty(id)) {
            return userService.getUserByName(name);
        } else {
            return userService.getUserById(id);
        }
    }

    @GetMapping("delUser")
    public Boolean delUser(String id) {
        return userService.delUserById(id);
    }
}
