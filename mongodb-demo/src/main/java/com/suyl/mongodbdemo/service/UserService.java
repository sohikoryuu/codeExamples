package com.suyl.mongodbdemo.service;

import com.suyl.mongodbdemo.bean.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {

    User getUserById(String id);
    List<User> getUserByName(String name);

    boolean InsUser(User user);

    boolean delUserById(String id);
}
