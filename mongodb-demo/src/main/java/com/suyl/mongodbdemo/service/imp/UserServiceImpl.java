package com.suyl.mongodbdemo.service.imp;

import com.suyl.mongodbdemo.bean.User;
import com.suyl.mongodbdemo.service.UserRepository;
import com.suyl.mongodbdemo.service.UserService;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@Component
public class UserServiceImpl implements UserService {

    @Resource
    UserRepository userRepository;
    @Resource
    MongoTemplate mongoTemplate;

    @Override
    public User getUserById(String id) {
        return userRepository.findById(id).get();
    }

    @Override
    public List<User> getUserByName(String name) {
        return userRepository.getUserByName(name);
    }

    @Override
    public boolean InsUser(User user) {
        user = userRepository.insert(user);
        return true;
    }

    @Override
    public boolean delUserById(String id) {
        userRepository.deleteById(id);
        return true;
    }
}
