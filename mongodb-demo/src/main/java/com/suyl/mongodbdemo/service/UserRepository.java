package com.suyl.mongodbdemo.service;

import com.suyl.mongodbdemo.bean.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface UserRepository extends MongoRepository<User, String> {

    @Override
    List<User> findAll();

    List<User> getUserByName(String name);
}
