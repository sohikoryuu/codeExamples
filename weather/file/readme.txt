天气查询

1.先查缓存
2.查redis
3.查数据库
4.调用天气查询接口

##########技术##########
springDataJPA
springBoot缓存
redis
mysql
hibernate 生成表结构
jwt
##########插件##########
lombok
contiperf性能测试
fastjson
httpclient远程连接
devtools 热部署

------------------------------------------
说明：
Hibernate创建sql语句字符集错误
Hibernate: drop table if exists auth_user
Hibernate: drop table if exists buss_forecast
Hibernate: drop table if exists buss_query_record
Hibernate: drop table if exists cons_city
Hibernate: create table auth_user (id bigint not null, account varchar(32), name varchar(32), pwd varchar(64), primary key (id)) engine=InnoDB
Hibernate: create table buss_forecast (id bigint not null auto_increment, aqi varchar(8), code varchar(10), create_date datetime, date varchar(4), f1 varchar(16), fx varchar(16), ganmao varchar(255), high varchar(32), low varchar(32), notice varchar(128), pm10 varchar(4), pm25 varchar(4), quality varchar(32), remarks varchar(255), shidu varchar(8), sunrise varchar(8), sunset varchar(8), type varchar(8), week varchar(8), wendu varchar(4), ymd varchar(16), primary key (id)) engine=InnoDB
Hibernate: create table buss_query_record (id bigint not null auto_increment, city varchar(32), city_key varchar(16), create_date datetime, date varchar(8), message varchar(255), parent varchar(32), remarks varchar(255), status varchar(5), time varchar(32), update_time varchar(8), primary key (id)) engine=InnoDB
Hibernate: create table cons_city (id bigint not null auto_increment, code varchar(10), create_date datetime, name varchar(32), remarks varchar(64), primary key (id)) engine=InnoDB

修改为：
drop table if exists auth_user;
drop table if exists buss_forecast;
drop table if exists buss_query_record;
drop table if exists cons_city;
create table auth_user (id bigint not null, account varchar(32), name varchar(32), pwd varchar(64), primary key (id)) engine=InnoDB,CHARSET=utf8;
create table buss_forecast (id bigint not null auto_increment, aqi varchar(8), code varchar(10), create_date datetime, date varchar(4), f1 varchar(16), fx varchar(16), ganmao varchar(255), high varchar(32), low varchar(32), notice varchar(128), pm10 varchar(4), pm25 varchar(4), quality varchar(32), remarks varchar(255), shidu varchar(8), sunrise varchar(8), sunset varchar(8), type varchar(8), week varchar(8), wendu varchar(4), ymd varchar(16), primary key (id)) engine=InnoDB,CHARSET=utf8;
create table buss_query_record (id bigint not null auto_increment, city varchar(32), city_key varchar(16), create_date datetime, date varchar(8), message varchar(255), parent varchar(32), remarks varchar(255), status varchar(5), time varchar(32), update_time varchar(8), primary key (id)) engine=InnoDB,CHARSET=utf8;
create table cons_city (id bigint not null auto_increment, code varchar(10), create_date datetime, name varchar(32), remarks varchar(64), primary key (id)) engine=InnoDB,CHARSET=utf8;