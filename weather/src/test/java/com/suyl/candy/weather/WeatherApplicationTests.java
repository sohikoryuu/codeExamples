package com.suyl.candy.weather;

import com.suyl.candy.weather.modules.business.dao.CityDao;
import com.suyl.candy.weather.modules.business.service.CityCodeService;
import com.suyl.candy.weather.modules.business.service.IDCardService;
import com.suyl.candy.weather.modules.business.service.WeatherService;
import com.suyl.candy.weather.modules.business.service.impl.HttpCallServiceImp;
import com.suyl.candy.weather.modules.sys.dao.UserDao;
import com.suyl.candy.weather.modules.sys.entity.UserDO;
import com.suyl.candy.weather.utils.RedisUtil;
import lombok.extern.log4j.Log4j2;
import org.databene.contiperf.PerfTest;
import org.databene.contiperf.junit.ContiPerfRule;
import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

@Log4j2
@SpringBootTest
class WeatherApplicationTests {

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @Autowired
    RedisTemplate redisTemplate;

    @Autowired
    RedisUtil redisUtil;

    @Autowired
    UserDao userDao;

    @Autowired
    CityDao cityDao;

    @Autowired
    WeatherService weatherService;

    @Autowired
    HttpCallServiceImp httpCallServiceImp;

    @Rule
    public ContiPerfRule i = new ContiPerfRule();

    /**
     * redis 测试
     */
    @Test
    void contextLoads() {
//        redisUtils.set("suyl", "12456");
//        redisService.
//        redisTemplate.opsForValue().set("suyl", "123456");
//        stringRedisTemplate.opsForValue().set("suyl", "666666");
//        System.out.println(stringRedisTemplate.opsForValue().get("suyl"));
//        RedisUtil redisUtil = new RedisUtil();
//        redisUtil.setRedisTemplate(redisTemplate);
//        System.out.println(redisUtil.get("suyl"));
//        Date date = new Date(2019, 10, 21, 9, 15);
//        redisTemplate.expireKeyAt("suyl", date);
//        redisTemplate.expireAt("suyl", date);
//        stringRedisTemplate.expireAt("suyl", date);

//        redisTemplate.opsForSet().add("set1", "set1");
//        redisTemplate.opsForSet().add("set2", "set2");
//        redisTemplate.opsForSet().add("set3", "set3");
//        System.out.println(redisTemplate.opsForSet().getOperations());

        redisTemplate.boundHashOps("set").put("石家庄", "101090101");
//        redisTemplate.expire("set", 1, TimeUnit.SECONDS);
//        redisTemplate.boundHashOps("set").put("set2","set2");
//        redisTemplate.boundHashOps("set").put("set3","set3");
        System.out.println(redisUtil.getBoundHashOps("set", "石家庄").toString());
//        redisUtil.expire("set1", 1);

        System.out.println("ok");
    }

    /**
     * spring data JPA 测试
     */
    @Test
    public void TestUser() {
        // 插入数据
        UserDO userDO = new UserDO();
        userDO.setId(1L);
        userDO.setName("苏彦龙");
        userDO.setPwd("123456");
        userDO.setAccount("suyanlong");
        userDao.save(userDO);
//        Optional<UserDO> userDO1 = userDao.findById(1L);

        // 查询
//        System.out.println(userDao.findByName("sohikoryuu").getPwd());
//        System.out.println(userDao.findByAccountLike("%yan%").getPwd());
//        System.out.println(userDao.findByNameOrName("sohikoryuu", "candy").get(1).getPwd());
//        System.out.println(userDao.findTwoName("sohikoryuu", "candy").get(1).getPwd());
//        System.out.println(userDao.findTwoNameSQL("sohikoryuu", "candy").get(1).getPwd());
    }

    /**
     * 性能测试
     *
     * @throws Exception
     */
    @org.junit.Test
    @PerfTest(invocations = 200000000, threads = 16)
    public void test1() throws Exception {
        System.out.println(1);
    }


    @Test
    public void AddTokenToRedis() {
        redisUtil.getBoundHashOps("citys");
        System.out.println(redisUtil.getBoundHashOps("citys", "平山"));
//        redisUtil.set("ceshi", "qwerdf", 10);
//        httpCallServiceImp.getWeather("http://t.weather.sojson.com/api/weather/city/101030300");
//        log.info(weatherService.searchWeather("101090101", "2019-10-28"));
//        SecureUtil.md5().
//        UserDO userDO = userDao.findByName("苏彦龙");
//
//        try {
//            String ab = JWTUtils.createJWT("user", userDO.getAccount(), 1000 * 60 * 60);
//
//            redisUtil.putBoundHashOps("users", userDO.getAccount(), ab);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    @Autowired
    IDCardService idCardService;

    @Autowired
    CityCodeService cityCodeService;

    @Test
    public void getIp() {
//        Object object = HttpClientUtils.get("https://site.ip138.com/domain/read.do?domain=suyllovecandy.top&time=" + System.currentTimeMillis());
//        System.out.println(object);

        log.info(idCardService.idCardAuth("130131199312161218"));
    }
}
