package com.suyl.candy.weather.modules.business.service.impl;

import com.suyl.candy.weather.modules.business.dao.CityCodeDao;
import com.suyl.candy.weather.modules.business.entity.CityCodeDO;
import com.suyl.candy.weather.modules.business.service.CityCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class CityCodeServiceImp implements CityCodeService {

    @Autowired
    CityCodeDao cityCodeDao;

    @Override
    public boolean save(List<CityCodeDO> cityCodeDOS) {
        boolean res = true;
        try {
            cityCodeDao.saveAll(cityCodeDOS);
        } catch (Exception e) {
            res = false;
        }
        return res;
    }

    @Override
    public CityCodeDO getOne(String id) {
        List<CityCodeDO> list = cityCodeDao.findAllById(Arrays.asList(id));
        return (list != null && list.size() > 0) ? list.get(0) : null;
    }
}
