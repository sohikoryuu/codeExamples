package com.suyl.candy.weather.modules.sys.controller;

import com.suyl.candy.weather.modules.sys.entity.UserDO;
import com.suyl.candy.weather.modules.sys.service.UserService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Log4j2
@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    UserService userService;

    @CrossOrigin
    @ResponseBody
    @GetMapping(value = "/name")
    public Object getWeather(HttpServletRequest req, HttpServletResponse resp) {
        UserDO userDO = null;
        try {
//            synchronized (this) {
                userDO = userService.findByName("candy");
                if (userDO != null) {
                    log.info("用户为" + userDO.getAccount());
                }
//            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return userDO;
    }
}
