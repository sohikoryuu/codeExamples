package com.suyl.candy.weather.example.ifelsereplace.factory;

import com.suyl.candy.weather.example.ifelsereplace.service.UserPayService;
import io.jsonwebtoken.lang.Assert;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class UserPayServiceStrategyFactory {
    private static Map<String, UserPayService> serviceMaps = new ConcurrentHashMap<>();

    public static UserPayService getByUserType(String userType) {
        return serviceMaps.get(userType);
    }

    public static void register(String userType, UserPayService userPayService) {
        Assert.notNull(userType, "userType can't be null");
        serviceMaps.put(userType, userPayService);
    }
}
