package com.suyl.candy.weather.modules.sys.service.impl;

import com.suyl.candy.weather.modules.sys.dao.UserDao;
import com.suyl.candy.weather.modules.sys.entity.UserDO;
import com.suyl.candy.weather.modules.sys.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserServiceImp implements UserService {

    @Autowired
    UserDao userDao;

    @Override
    public UserDO findByName(String name) {
        return userDao.findByName(name);
    }
}
