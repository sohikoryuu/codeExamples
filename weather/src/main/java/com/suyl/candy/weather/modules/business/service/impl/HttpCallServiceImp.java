package com.suyl.candy.weather.modules.business.service.impl;

import cn.miludeer.jsoncode.JsonCode;
import com.alibaba.fastjson.JSONObject;
import com.suyl.candy.weather.modules.business.dao.ForecastDao;
import com.suyl.candy.weather.modules.business.dao.QueryRecordDao;
import com.suyl.candy.weather.modules.business.entity.ForecastDO;
import com.suyl.candy.weather.modules.business.entity.QueryRecordDO;
import com.suyl.candy.weather.utils.DateUtils;
import com.suyl.candy.weather.utils.HttpClientUtils;
import com.suyl.candy.weather.utils.RedisUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@Component
public class HttpCallServiceImp {

    @Autowired
    ForecastDao forecastDao;

    @Autowired
    QueryRecordDao queryRecordDao;

    @Autowired
    RedisUtil redisUtil;

    @Transactional
    public Object getWeather(String url) {
        ForecastDO forecastDO = null;
        try {
            Object object = HttpClientUtils.get(url);
            if (object != null) {
                // JSONObject jsonObject = JSONObject.parseObject(object.toString());
                String objectString = object.toString();
                // 查询记录
                QueryRecordDO queryRecordDO = new QueryRecordDO();
                queryRecordDO.setCity(get(objectString, "$.cityInfo.city"));
                queryRecordDO.setCityKey(get(objectString, "$.cityInfo.citykey"));
                queryRecordDO.setParent(get(objectString, "$.cityInfo.parent"));
                queryRecordDO.setUpdateTime(get(objectString, "$.cityInfo.updateTime"));
                queryRecordDO.setCreateDate(DateUtils.date());
                queryRecordDO.setDate(get(objectString, "$.date"));
                queryRecordDO.setMessage(get(objectString, "$.message"));
                queryRecordDO.setStatus(get(objectString, "$.status"));
                queryRecordDO.setTime(get(objectString, "$.time"));
                queryRecordDao.save(queryRecordDO);

                Long qrId = queryRecordDO.getId();
                String qrCode = queryRecordDO.getCityKey();
                List<ForecastDO> forecastDOList = JSONObject.parseArray(get(objectString, "$.data.forecast"), ForecastDO.class);
                ForecastDO forecastDOYesterday = JSONObject.parseObject(get(objectString, "$.data.yesterday"), ForecastDO.class);

                forecastDOList.get(0).setGanmao(get(objectString, "$.data.ganmao"));
                forecastDOList.get(0).setPm10(get(objectString, "$.data.pm10"));
                forecastDOList.get(0).setPm25(get(objectString, "$.data.pm25"));
                forecastDOList.get(0).setQuality(get(objectString, "$.data.quality"));
                forecastDOList.get(0).setShidu(get(objectString, "$.data.shidu"));
                forecastDOList.get(0).setWendu(get(objectString, "$.data.wendu"));

                forecastDOList.add(forecastDOYesterday);

                forecastDOList.stream().filter(s -> {
                    s.setGid(qrId);
                    s.setCode(qrCode);
                    s.setCreateDate(new Date());
                    return true;
                }).collect(Collectors.toList());
                forecastDao.saveAll(forecastDOList);

                forecastDO = forecastDOList.get(0);
                redisUtil.putBoundHashOps(forecastDO.getCode(), forecastDO.getYmd(), JSONObject.toJSONString(forecastDO));
                redisUtil.expire(forecastDO.getCode(), 10);
                int all = forecastDOList.size() - 2;
                for (int i = 1; i < all; i++) {
                    redisUtil.set(forecastDOList.get(i).getCode() + forecastDOList.get(i).getYmd(), JSONObject.toJSONString(forecastDOList.get(i)), 5 * (all - i));

                }
            }
        } catch (Exception e) {
            log.error("远程调用【天气接口】查询失败");
        }
        return forecastDO;
    }

    private String get(String obj, String key) {
        return JsonCode.getValue(obj, key);
    }
}
