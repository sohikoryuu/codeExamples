package com.suyl.candy.weather.modules.sys.service;

import com.suyl.candy.weather.modules.sys.entity.UserDO;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * spring boot + redis 缓存
 * 参考：https://www.jianshu.com/p/005590753902
 */
@Service
@CacheConfig(cacheNames = {"users"})
public interface UserService {

    @Cacheable(key = "#p0")
    UserDO findByName(String name);
}
