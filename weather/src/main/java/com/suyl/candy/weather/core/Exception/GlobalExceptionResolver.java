//package com.suyl.candy.weather.core.Exception;
//
//import lombok.extern.java.Log;
//import org.springframework.stereotype.Component;
//import org.springframework.web.servlet.HandlerExceptionResolver;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//
//@Log
//@Component
//public class GlobalExceptionResolver implements HandlerExceptionResolver {
//
//    public static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:sss");
//
//    public static StringBuilder stringBuilder = new StringBuilder();
//
//    @Override
//    public ModelAndView resolveException(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) {
//        log.info("异常时间：" + simpleDateFormat.format(new Date()));
//        log.info("请求地址：" + httpServletRequest.getRequestURL());
//        log.info("异常原因：" + e.toString());
//
//        return new ModelAndView("error");
//    }
//}

package com.suyl.candy.weather.core.Exception;

import lombok.extern.java.Log;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 全局异常处理
 */
@Log
@RestControllerAdvice
public class GlobalExceptionResolver {

    public static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:sss");

    public static StringBuilder stringBuilder = new StringBuilder();

    //    @ResponseBody
    @ExceptionHandler(Exception.class)
    public Object resolveException(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) {
        log.info("异常时间：" + simpleDateFormat.format(new Date()));
        log.info("请求地址：" + httpServletRequest.getRequestURL());
        log.info("异常原因：" + e.toString());

        return new ModelAndView("error");
    }
}
