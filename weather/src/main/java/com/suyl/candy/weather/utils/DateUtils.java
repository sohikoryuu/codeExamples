package com.suyl.candy.weather.utils;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 日期工具类, 继承org.apache.commons.lang3.time.DateUtils类
 *
 * @author
 * @version V 1.0
 * @date 2016-12-21
 */

public class DateUtils {

    private static String[] parsePatterns = {"yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm", "yyyy-MM",
            "yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm", "yyyy/MM", "yyyy.MM.dd", "yyyy.MM.dd HH:mm:ss",
            "yyyy.MM.dd HH:mm", "yyyy.MM", "EEE MMM dd HH:mm:ss zzz yyyy"};

    private static String[] numbers = {"零", "一", "二", "三", "四", "五", "六", "七", "八", "九", "十", "十一", "十二", "十三", "十四", "十五", "十六", "十七", "十八", "十九", "二十", "二十一", "二十二", "二十三", "二十四", "二十五", "二十六", "二十七", "二十八", "二十九", "三十", "三十一"};
    public static String[] numbersChinese = {"零", "一", "二", "三", "四", "五", "六", "七", "八", "九", "十", "十一", "十二", "十三", "十四", "十五", "十六", "十七", "十八", "十九", "二十", "二十一", "二十二", "二十三", "二十四", "二十五", "二十六", "二十七", "二十八", "二十九", "三十", "三十一"};

    /**
     * Mon Aug 19 16:00:00 CST 2019字符串转为Date
     *
     * @param str
     * @return
     * @throws ParseException
     */
    public static Date getStrToDate(String str) throws ParseException {
        return new SimpleDateFormat(parsePatterns[12], Locale.US).parse(str);
    }

    /**
     * 得到中文月份
     *
     * @param month
     * @return
     */
    public static String getChineseMonth(String month) {
        return DateUtils.numbersChinese[Integer.parseInt(month) + 1] + "月";
    }

    /**
     * 得到中文周
     *
     * @param week
     * @return
     */
    public static String getChineseWeek(String week) {
        return "第" + DateUtils.numbersChinese[Integer.parseInt(week)] + "周";
    }

    /**
     * 得到当前日期字符串 格式（yyyy-MM-dd）
     */
    public static String getDate() {
        return getDate("yyyy-MM-dd");
    }

    /**
     * 得到当前日期 格式（yyyy-MM-dd）
     */
    public static Date date() {
        return new Date();
    }

    /**
     * 得到当前日期字符串 格式（yyyy-MM-dd） pattern可以为："yyyy-MM-dd" "HH:mm:ss" "E"
     */
    public static String getDate(String pattern) {
        return DateFormatUtils.format(new Date(), pattern);
    }

    public static String getChineseDate(Date date) {
        String year = DateFormatUtils.format(date, "yyyy");
        String month = DateFormatUtils.format(date, "MM");
        String day = DateFormatUtils.format(date, "dd");
        StringBuilder sy = new StringBuilder();
        for (int i = 0; i < year.length(); i++) {
            sy.append(numbers[Integer.parseInt(year.substring(i, i + 1))]);
        }
        StringBuilder sm = new StringBuilder();
        sm.append(numbers[Integer.parseInt(month)]);

        StringBuilder sd = new StringBuilder();
        sd.append(numbers[Integer.parseInt(day)]);

        return sy.append("年").append(sm.toString()).append("月").append(sd.toString()).append("日").toString();
    }

    /**
     * 获取本个月第一天
     *
     * @return
     */
    public static Date getNowMonthFirstDay() {
        Calendar cal = Calendar.getInstance(); //获取当前日期
        cal.add(Calendar.MONTH, 0);
        cal.set(Calendar.DAY_OF_MONTH, 1); //设置为1号,当前日期既为本月第一天
        Date result = cal.getTime();
        return result;
    }

    /**
     * 设置某天日期 且几点
     *
     * @param date  2019-04-24
     * @param point 5
     * @return 2019-04-24 05:00:00
     */
    public static Date getTimePipe(String date, int point) {
        Date date1 = stringToDate(date, parsePatterns[0]);
        date1.setHours(point);
        return date1;
    }

    /**
     * 得到日期字符串 默认格式（yyyy-MM-dd） pattern可以为："yyyy-MM-dd" "HH:mm:ss" "E"
     */
    public static String formatDate(Date date, Object... pattern) {
        String formatDate = null;
        if (pattern != null && pattern.length > 0) {
            formatDate = DateFormatUtils.format(date, pattern[0].toString());
        } else {
            formatDate = DateFormatUtils.format(date, "yyyy-MM-dd");
        }
        return formatDate;
    }

    /**
     * 得到日期时间字符串，转换格式（yyyy-MM-dd HH:mm:ss）
     */
    public static String formatDateTime(Date date) {
        return formatDate(date, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 得到当前时间字符串 格式（HH:mm:ss）
     */
    public static String getTime() {
        return formatDate(new Date(), "HH:mm:ss");
    }

    /**
     * 得到当前日期和时间字符串 格式（yyyy-MM-dd HH:mm:ss）
     */
    public static String getDateTime() {
        return formatDate(new Date(), "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 得到当前年份字符串 格式（yyyy）
     */
    public static String getYear() {
        return formatDate(new Date(), "yyyy");
    }

    /**
     * 得到当前月份字符串 格式（MM）
     */
    public static String getMonth() {
        return formatDate(new Date(), "MM");
    }

    /**
     * 得到当天字符串 格式（dd）
     */
    public static String getDay() {
        return formatDate(new Date(), "dd");
    }

    /**
     * 得到当前星期字符串 格式（E）星期几
     */
    public static String getWeek() {
        return formatDate(new Date(), "E");
    }

    /**
     * 日期型字符串转化为日期 格式 { "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm",
     * "yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm", "yyyy.MM.dd",
     * "yyyy.MM.dd HH:mm:ss", "yyyy.MM.dd HH:mm" }
     */
    public static Date parseDate(Object str) {
        if (str == null) {
            return null;
        }
        try {
            return org.apache.commons.lang.time.DateUtils.parseDate(str.toString(), parsePatterns);
        } catch (ParseException e) {
            e.printStackTrace();

        }
        return null;
    }

    /**
     * 获取过去的天数
     *
     * @param date
     * @return
     */
    public static long pastDays(Date date) {
        long t = new Date().getTime() - date.getTime();
        return t / (24 * 60 * 60 * 1000);
    }

    /**
     * 获取过去的小时
     *
     * @param date
     * @return
     */
    public static long pastHour(Date date) {
        long t = new Date().getTime() - date.getTime();
        return t / (60 * 60 * 1000);
    }

    /**
     * 获取过去的分钟
     *
     * @param date
     * @return
     */
    public static long pastMinutes(Date date) {
        long t = new Date().getTime() - date.getTime();
        return t / (60 * 1000);
    }

    /**
     * 转换为时间（天,时:分:秒.毫秒）
     *
     * @param timeMillis
     * @return
     */
    public static String formatDateTime(long timeMillis) {
        long day = timeMillis / (24 * 60 * 60 * 1000);
        long hour = (timeMillis / (60 * 60 * 1000) - day * 24);
        long min = ((timeMillis / (60 * 1000)) - day * 24 * 60 - hour * 60);
        long s = (timeMillis / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
        long sss = (timeMillis - day * 24 * 60 * 60 * 1000 - hour * 60 * 60 * 1000 - min * 60 * 1000 - s * 1000);
        return (day > 0 ? day + "," : "") + hour + ":" + min + ":" + s + "." + sss;
    }

    /**
     * 获取两个日期之间的天数
     *
     * @param before
     * @param after
     * @return
     */
    public static double getDistanceOfTwoDate(Date before, Date after) {
        long beforeTime = before.getTime();
        long afterTime = after.getTime();
        return (afterTime - beforeTime) / (1000 * 60 * 60 * 24);
    }

    /**
     * 将一个时间戳转换成提示性时间字符串，如刚刚，1秒前
     *
     * @param before
     * @return
     */
    public static String convertTimeToFormat(Date before) {
        long curTime = System.currentTimeMillis() / (long) 1000;
        long time = curTime - before.getTime();

        if (time < 60 && time >= 0) {
            return "刚刚";
        } else if (time >= 60 && time < 3600) {
            return time / 60 + "分钟前";
        } else if (time >= 3600 && time < 3600 * 24) {
            return time / 3600 + "小时前";
        } else if (time >= 3600 * 24 && time < 3600 * 24 * 30) {
            return time / 3600 / 24 + "天前";
        } else if (time >= 3600 * 24 * 30 && time < 3600 * 24 * 30 * 12) {
            return time / 3600 / 24 / 30 + "个月前";
        } else if (time >= 3600 * 24 * 30 * 12) {
            return time / 3600 / 24 / 30 / 12 + "年前";
        } else {
            return "刚刚";
        }
    }

    /**
     * string装date
     *
     * @param dateStr    日期字符串
     * @param dateFormat 日期字符串
     * @return 日期
     */
    public static Date stringToDate(String dateStr, String dateFormat) {
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        try {
            return sdf.parse(dateStr);
        } catch (ParseException ex) {
        }
        return new Date();
    }

    /**
     * 得到两个日期所有天数list
     *
     * @param date1 2019-04-22
     * @param date2 2019-04-24
     * @return [2019-04-22，2019-04-23，2019-04-24]
     */
    public static List<String> getTwoDateMidAllDay(String date1, String date2) {
        List<String> dateList = new ArrayList<>();
        if (StringUtils.isNotEmpty(date1) && StringUtils.isNotEmpty(date2)) {
            if (date1.equals(date2)) {
                dateList.add(date2);
            } else {
                if (stringToDate(date1, "yyyy-MM-dd").getTime() > stringToDate(date2, "yyyy-MM-dd").getTime()) {
                    String temp = date1;
                    date1 = date2;
                    date2 = temp;
                }
                for (; !date1.equals(date2); ) {
                    dateList.add(date1);
                    date1 = formatDate(new Date(stringToDate(date1, "yyyy-MM-dd").getTime() + 1000 * 60 * 60 * 24));
                }
                dateList.add(date2);
            }
        }
        return dateList;
    }

    /**
     * 两个时间比较
     *
     * @param date1
     * @param date2
     * @return 前者大为true、后者大为false
     */
    public static boolean compareDate(Date date1, Date date2) {
        return date1.getTime() >= date2.getTime();
    }

    /**
     * @param args
     * @throws ParseException
     */
    public static void main(String[] args) throws ParseException {
        // System.out.println(formatDate(parseDate("2010/3/6")));
        // System.out.println(getDate("yyyy年MM月dd日 E"));
        // long time = new Date().getTime()-parseDate("2012-11-19").getTime();
        // System.out.println(time/(24*60*60*1000));

        String pattern = "^\\d{1,5}(\\.\\d{1,3})?$";
        System.out.println("-1111".matches(pattern));

        System.out.println(getChineseDate(new Date()));
        System.out.println(getDate());

        System.out.println(formatDate(getTimePipe("2019-04-22", 0), "yyyy-MM-dd HH:mm:ss"));

        System.out.println(getTwoDateMidAllDay("", "2019-03-24"));
    }
}
