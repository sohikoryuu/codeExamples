package com.suyl.candy.weather.modules.business.controller;

import com.suyl.candy.weather.modules.business.service.CityService;
import com.suyl.candy.weather.utils.RedisUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Log4j2
@RestController
@RequestMapping("city")
public class CityController {

    @Autowired
    CityService cityService;

    @Autowired
    RedisUtil redisUtil;

    @CrossOrigin
    @ResponseBody
    @GetMapping(value = "/getAll")
    public Object getWeather(HttpServletRequest req, HttpServletResponse resp) {
        Map<String, Object> result = new HashMap<>();
        Set<Object> keySet = null;
        try {
            Map<Object, Object> mapCity = redisUtil.hmget("citys");
            keySet = mapCity.keySet();
            log.info(mapCity);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        result.put("success", true);
        result.put("data", keySet);
        return result;
    }
}
