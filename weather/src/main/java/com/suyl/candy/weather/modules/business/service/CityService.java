package com.suyl.candy.weather.modules.business.service;

import com.suyl.candy.weather.modules.business.entity.CityDO;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * spring boot + redis 缓存
 * 参考：https://www.jianshu.com/p/005590753902
 */
@Service
@CacheConfig(cacheNames = {"citys"})
public interface CityService {

    @Cacheable(key = "#p0")
    CityDO findByName(String name);

    List<Long> findAllByCityZhEQLeaderZh();

    List<Long> findAllId();
}
