package com.suyl.candy.weather.modules.sys.dao;

import com.suyl.candy.weather.modules.sys.entity.UserDO;
import io.lettuce.core.dynamic.annotation.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 参考：https://www.jianshu.com/p/c14640b63653
 */
@Repository
public interface UserDao extends JpaRepository<UserDO, Long> {

    /**
     * jpa 查询
     *
     * @param name
     * @return
     */
    UserDO findByName(String name);

    UserDO findByAccountLike(String account);

    List<UserDO> findByNameOrName(String name1, String name2);

    /**
     * 自定义查询 PQL语法
     *
     * @param name1
     * @param name2
     * @return
     */
    @Query("select O from UserDO O WHERE O.name = ?1 or O.name = ?2")
//    @Query("select O from UserDO O WHERE O.name = :name1 or O.name = :name2") // 不知道为啥不行
    List<UserDO> findTwoName(@Param("name1") String name1, @Param("name2") String name2);

    /**
     * 自定义查询 SQL语法
     *
     * @param name1
     * @param name2
     * @return
     */
    @Query(nativeQuery = true, value = "SELECT * FROM auth_user WHERE NAME = ?1 OR NAME = ?2")
    List<UserDO> findTwoNameSQL(@Param("name1") String name1, @Param("name2") String name2);
}
