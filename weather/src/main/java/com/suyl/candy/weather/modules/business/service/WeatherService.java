package com.suyl.candy.weather.modules.business.service;


import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
@CacheConfig(cacheNames = {"weather"})
public interface WeatherService {

    @Cacheable(key = "#p0 + #p1")
    Object searchWeather(String code, String date);
}
