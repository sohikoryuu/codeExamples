package com.suyl.candy.weather.modules.business.dao;


import com.suyl.candy.weather.modules.business.entity.QueryRecordDO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QueryRecordDao extends JpaRepository<QueryRecordDO, Long> {
}
