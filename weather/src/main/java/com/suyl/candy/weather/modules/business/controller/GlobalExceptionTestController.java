package com.suyl.candy.weather.modules.business.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("search")
public class GlobalExceptionTestController {

    @CrossOrigin
    @GetMapping("test")
    public String test() {
        return String.valueOf(1 / 0);
//        return "error";
    }
}
