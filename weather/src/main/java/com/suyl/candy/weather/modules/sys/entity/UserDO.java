package com.suyl.candy.weather.modules.sys.entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * lombok 插件使用
 * 参考：https://blog.csdn.net/qq_33404395/article/details/80656654
 */
@Data
@ToString
@Entity
@Table(name = "auth_user")
public class UserDO implements Serializable {

    public static final long serialVersionUID = -624145366487777133L;

    @Id
    private Long id;
    @Column(length = 32)
    private String name;
    @Column(length = 32)
    private String account;
    @Column(length = 64)
    private String pwd;
}
