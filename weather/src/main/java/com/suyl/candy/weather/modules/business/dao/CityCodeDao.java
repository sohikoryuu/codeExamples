package com.suyl.candy.weather.modules.business.dao;

import com.suyl.candy.weather.modules.business.entity.CityCodeDO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * 参考：https://www.jianshu.com/p/c14640b63653
 */
@Repository
public interface CityCodeDao extends JpaRepository<CityCodeDO, String> {

}
