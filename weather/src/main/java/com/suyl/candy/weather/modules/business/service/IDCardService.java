package com.suyl.candy.weather.modules.business.service;

import com.suyl.candy.weather.modules.business.entity.IDCardDO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public interface IDCardService {

    @Transactional
    boolean save(IDCardDO... idCardDOS);

    Object idCardAuth(String idCardNo);
}
