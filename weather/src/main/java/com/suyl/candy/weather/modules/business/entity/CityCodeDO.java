package com.suyl.candy.weather.modules.business.entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@ToString
@Entity
@Table(name = "cons_citycode")
public class CityCodeDO implements Serializable {

    public static final long serialVersionUID = -624145366487777113L;

    /**
     * 用户id，主键自增
     */
    @Id
    @Column(length = 6)
    private String id;

    /**
     * 城市名字
     */
    @Column(length = 36)
    private String name;
}
