package com.suyl.candy.weather.modules.business.dao;

import com.suyl.candy.weather.modules.business.entity.CityDO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 参考：https://www.jianshu.com/p/c14640b63653
 */
@Repository
public interface CityDao extends JpaRepository<CityDO, Long> {

    CityDO findByCityEn(String name);

    CityDO findById(String code);

    @Query("select id from CityDO where cityZh = leaderZh ")
    List<Long> findAllByCityZhEQLeaderZh();

    @Query("select id from CityDO ")
    List<Long> findAllId();
}
