package com.suyl.candy.weather.example.reflect;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

public class ReflectTemp1 {
    public static void main(String[] args) {
        Book book = new Book();
        book.setId(12);
        book.setName("candy");
        String objectBook = JSONObject.toJSONString(book);
        System.out.println(objectBook);

        Book book1 = JSONObject.parseObject(objectBook, Book.class);
        System.out.println(book1.getId());
        System.out.println(book1.getName());

        List<String> stringList = new ArrayList<>();
        List<Integer> integerList = new ArrayList<>();
        System.out.println(stringList.getClass().equals(integerList.getClass()) ? "泛型相同" : "泛型不同");
        System.out.println(stringList.getClass());
    }

    @Data
    public static class Book {
        private int id;
        private String name;
    }

    @Data
    public static class User {
        private String name;
        private String idCard;
    }
}
