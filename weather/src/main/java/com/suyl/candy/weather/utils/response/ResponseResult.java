package com.suyl.candy.weather.utils.response;

import java.io.Serializable;
import java.util.Map;

public class ResponseResult implements Serializable {

    private static final long serialVersionUID = -999362211833007653L;

    /**
     * 是否成功，成功找data、失败找msg
     */
    protected boolean success;

    /**
     * 返回结果编码，成功的话我喜欢设为0
     */
    protected String code;

    /**
     * 返回消息，一般放置可追溯的错误消息
     */
    protected String msg;

    /**
     * 返回数据
     */
    protected Object data;

    /**
     * 额外参数
     */
    protected Map<String, Object> extraInfo;

    private ResponseResult(boolean success, String code, String msg, Object data, Map<String, Object> extraInfo) {
        this.success = success;
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.extraInfo = extraInfo;
    }

    public static ResponseResult success(Object data, Map<String, Object> extraInfo) {
        return new ResponseResult(true, ReturnCode.SUCCESS.getCode(), ReturnCode.SUCCESS.getMsg(), data, extraInfo);
    }

    public static ResponseResult fail(Map<String, Object> extraInfo) {
        return new ResponseResult(false, ReturnCode.FEAILED.getCode(), ReturnCode.FEAILED.getMsg(), null, extraInfo);
    }
}
