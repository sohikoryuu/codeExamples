package com.suyl.candy.weather.modules.business.service;

import com.suyl.candy.weather.modules.business.entity.CityCodeDO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public interface CityCodeService {

    boolean save(List<CityCodeDO> cityCodeDOS);

    CityCodeDO getOne(String id);
}
