package com.suyl.candy.weather.modules.business.dao;

import com.suyl.candy.weather.modules.business.entity.ForecastDO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * 参考：https://www.jianshu.com/p/c14640b63653
 */
@Repository
public interface ForecastDao extends JpaRepository<ForecastDO, Long> {

    List<ForecastDO> findByCodeAndYmdAndCreateDateBetweenOrderByCreateDateDesc(String code, String ymd, Date date1, Date date2);

    List<ForecastDO> findByCodeAndYmdAndCreateDateBeforeOrderByCreateDateDesc(String code, String ymd, Date date);


//    ForecastDO findByCode(String code);
}
