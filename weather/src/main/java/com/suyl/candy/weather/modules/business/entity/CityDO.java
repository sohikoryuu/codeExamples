package com.suyl.candy.weather.modules.business.entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@ToString
@Entity
@Table(name = "cons_citys")
public class CityDO implements Serializable {

    public static final long serialVersionUID = -624145366487777113L;

    /**
     * 用户id，主键自增
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 创建时间
     */
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    /**
     * 城市名字英文
     */
    @Column(length = 32)
    private String cityEn;

    /**
     * 城市名字中文
     */
    @Column(length = 32)
    private String cityZh;
    /**
     * 城市名字英文
     */
    @Column(length = 32)
    private String provinceEn;

    /**
     * 城市名字中文
     */
    @Column(length = 32)
    private String provinceZh;
    /**
     * 国家名字英文
     */
    @Column(length = 32)
    private String countryEn;

    /**
     * 国家名字中文
     */
    @Column(length = 32)
    private String countryZh;
    /**
     * 省会名字英文
     */
    @Column(length = 32)
    private String leaderEn;

    /**
     * 省会名字中文
     */
    @Column(length = 32)
    private String leaderZh;
    /**
     * 经度
     */
    @Column(length = 32)
    private String lat;

    /**
     * 纬度
     */
    @Column(length = 32)
    private String lon;


}
