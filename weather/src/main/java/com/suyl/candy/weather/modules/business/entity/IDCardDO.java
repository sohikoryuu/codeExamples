package com.suyl.candy.weather.modules.business.entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@ToString
@Entity
@Table(name = "buss_idcard")
public class IDCardDO implements Serializable {

    /**
     * 身份证号 唯一不重
     */
    @Id
    private String id;
    /**
     * 创建时间
     */
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    /**
     * 性别
     */
    @Column(length = 4)
    private String sex;

    /**
     * 出生日期
     */
    @Column(length = 16)
    private String birthday;

    /**
     * 省份
     */
    @Column(length = 16)
    private String province;

    /**
     * 市
     */
    @Column(length = 16)
    private String city;

    /**
     * 县
     */
    @Column(length = 16)
    private String county;
}
