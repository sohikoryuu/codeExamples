package com.suyl.candy.weather.core.scheduled;

import com.suyl.candy.weather.modules.business.service.CityService;
import com.suyl.candy.weather.modules.business.service.impl.HttpCallServiceImp;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Log4j2
@Component
public class SchedulLoadWeather {

    @Value("${http.client.weather.url1}")
    private String httpCallUrl;

    @Autowired
    HttpCallServiceImp httpCallServiceImp;

    @Autowired
    CityService cityService;

    @Scheduled(cron = "0 0 0/6 * * ?")
    public void fixedRate() throws InterruptedException {
//        List<Long> cityCodes = cityService.findAllByCityZhEQLeaderZh(); // 只有市
        List<Long> cityCodes = cityService.findAllId(); // 区县都有
        for (Long cityCode : cityCodes) {
            Object result = httpCallServiceImp.getWeather(httpCallUrl + cityCode);
            if (result != null)
                log.info("【cityCode=" + cityCode + "】天气查询成功！！！");
            Thread.sleep(6000);
        }
    }
}
