package com.suyl.candy.weather.example.ifelsereplace.service.impl;

import com.suyl.candy.weather.example.ifelsereplace.factory.UserPayServiceStrategyFactory;
import com.suyl.candy.weather.example.ifelsereplace.service.UserPayService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class ParticularlyVipPayService implements UserPayService, InitializingBean {
    @Override
    public BigDecimal quote(BigDecimal orderPrice) {
        return null;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        UserPayServiceStrategyFactory.register("ParticularlyVip", this);
    }
}
