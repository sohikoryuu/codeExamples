package com.suyl.candy.weather.modules.business.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.suyl.candy.weather.modules.business.dao.ForecastDao;
import com.suyl.candy.weather.modules.business.dao.QueryRecordDao;
import com.suyl.candy.weather.modules.business.entity.ForecastDO;
import com.suyl.candy.weather.modules.business.service.WeatherService;
import com.suyl.candy.weather.utils.DateUtils;
import com.suyl.candy.weather.utils.RedisUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;

@Log4j2
@Component
public class WeatherServiceImp implements WeatherService {

    @Autowired
    ForecastDao forecastDao;

    @Autowired
    QueryRecordDao queryRecordDao;

    @Autowired
    RedisUtil redisUtil;

    @Autowired
    HttpCallServiceImp httpCallServiceImp;

    @Value("${http.client.weather.url1}")
    private String httpCallUrl;

    /**
     * 查询某城市某天天气
     *
     * @param code 城市code
     * @param date 日期 2019-10-28
     * @return
     */
    @Override
    public Object searchWeather(String code, String date) {
        log.info("【缓存中】没有命中此条记录 code:" + code + "date:" + date);
        // redis 永久存储中查询
        Object redisValue = redisUtil.getBoundHashOps(code, date);
        if (redisValue != null) {
            return JSONObject.parseObject(redisValue.toString());
        }

        log.info("【redis 永久存储中】没有命中此条记录 code:" + code + "date:" + date);
        // redis 临时存储中查询
        Object object = redisUtil.get(code + date);
        if (!StringUtils.isEmpty(object)) {
            return JSONObject.parseObject(object.toString());
        }

        log.info("【redis 临时存储中】没有命中此条记录 code:" + code + "date:" + date);
        // 查询当天及之前天气
        // 当前时间大于等于 查询时间
        if (DateUtils.compareDate(DateUtils.getTimePipe(DateUtils.getDate(), 0), DateUtils.getTimePipe(date, 0))) {
            //数据库中查询 当天存当天预报
            Date date1 = DateUtils.parseDate(date);
            Date date2 = DateUtils.parseDate(date + " 23:59:59");
            List<ForecastDO> forecastDOs = forecastDao.findByCodeAndYmdAndCreateDateBetweenOrderByCreateDateDesc(code, date, date1, date2);
            if (!StringUtils.isEmpty(forecastDOs) && forecastDOs.size() > 0) {
                return forecastDOs.get(0);
            }

            log.info("【数据库中 当天天气预报】没有命中此条记录 code:" + code + "date:" + date);
            // 调用天气接口查询天气
            return httpCallServiceImp.getWeather(httpCallUrl + code);
        }

        // 查询未来天气
        Date date3 = DateUtils.parseDate(date);
        List<ForecastDO> forecastDOList = forecastDao.findByCodeAndYmdAndCreateDateBeforeOrderByCreateDateDesc(code, date, date3);
        if (forecastDOList != null && forecastDOList.size() > 0) {
            return JSONObject.toJSONString(forecastDOList.get(0));
        }
        log.info("【数据库中 未来天气预报】没有命中此条记录 code:" + code + "date:" + date);
        return redisValue;
    }
}
