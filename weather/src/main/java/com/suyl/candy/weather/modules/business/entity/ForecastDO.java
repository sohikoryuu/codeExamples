package com.suyl.candy.weather.modules.business.entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@ToString
@Entity
@Table(name = "buss_forecast")
public class ForecastDO implements Serializable {

    /**
     * buss_query_record 表 id
     */
    @Column
    private Long gid;
    /**
     * 用户id，主键自增
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 创建时间
     */
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    @Column(length = 8)
    private String aqi;

    /**
     * 日期
     */
    @Column(length = 4)
    private String date;

    /**
     * 风力
     */
    @Column(length = 16)
    private String fl;

    /**
     * 风向
     */
    @Column(length = 16)
    private String fx;

    /**
     * 最高温度
     */
    @Column(length = 32)
    private String high;

    /**
     * 最低温度
     */
    @Column(length = 32)
    private String low;

    /**
     * 祝福语
     */
    @Column(length = 128)
    private String notice;

    /**
     * 日出
     */
    @Column(length = 8)
    private String sunrise;

    /**
     * 日落
     */
    @Column(length = 8)
    private String sunset;

    /**
     * 天气情况
     */
    @Column(length = 8)
    private String type;

    /**
     * 周几
     */
    @Column(length = 8)
    private String week;

    /**
     * 日期
     */
    @Column(length = 16)
    private String ymd;

    /**
     * 感冒提示
     */
    @Column
    private String ganmao;

    /**
     * pm10
     */
    @Column(length = 16)
    private String pm10;

    /**
     * pm25
     */
    @Column(length = 16)
    private String pm25;

    /**
     * 空气质量
     */
    @Column(length = 32)
    private String quality;

    /**
     * 湿度
     */
    @Column(length = 8)
    private String shidu;

    /**
     * 温度
     */
    @Column(length = 4)
    private String wendu;

    /**
     * 城市代码
     */
    @Column(length = 10)
    private String code;

    /**
     * 备注
     */
    @Column
    private String remarks;
}
