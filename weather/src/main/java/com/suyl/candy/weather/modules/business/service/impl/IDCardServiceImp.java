package com.suyl.candy.weather.modules.business.service.impl;

import com.suyl.candy.weather.modules.business.dao.IDCardDao;
import com.suyl.candy.weather.modules.business.entity.CityCodeDO;
import com.suyl.candy.weather.modules.business.entity.IDCardDO;
import com.suyl.candy.weather.modules.business.service.CityCodeService;
import com.suyl.candy.weather.modules.business.service.IDCardService;
import com.suyl.candy.weather.utils.IDCard;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Component
public class IDCardServiceImp implements IDCardService {

    @Autowired
    IDCardDao idCardDao;

    @Autowired
    CityCodeService cityCodeService;

    @Override
    public boolean save(IDCardDO... idCardDOS) {
        boolean result = true;
        try {
            List list = Arrays.asList(idCardDOS);
            idCardDao.saveAll(list);
        } catch (Exception e) {
            result = false;
        }
        return result;
    }

    @Override
    public Object idCardAuth(String idCardNo) {
        String isIdCard = IDCard.IDCardValidate(idCardNo);
        if ("YES".equals(isIdCard)) {
            String addSign = idCardNo.substring(0, 6);
            CityCodeDO cityCodeDO = cityCodeService.getOne(addSign);
            IDCardDO idCardDO = new IDCardDO();
            idCardDO.setId(idCardNo);
            idCardDO.setBirthday(IDCard.IDCardBirthday(idCardNo));
            idCardDO.setCity(cityCodeService.getOne(addSign.substring(0, 4) + "00").getName());
            idCardDO.setCounty(cityCodeDO.getName());
            idCardDO.setCreateDate(new Date());
            idCardDO.setProvince(cityCodeService.getOne(addSign.substring(0, 2) + "0000").getName());
            idCardDO.setSex(IDCard.IDCardSex(idCardNo));
            save(idCardDO);
            return cityCodeDO;
        }
        return null;
    }
}
