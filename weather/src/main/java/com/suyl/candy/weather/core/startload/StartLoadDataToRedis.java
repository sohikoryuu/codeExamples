package com.suyl.candy.weather.core.startload;

import com.alibaba.fastjson.JSONObject;
import com.suyl.candy.weather.modules.business.dao.CityDao;
import com.suyl.candy.weather.modules.business.entity.CityDO;
import com.suyl.candy.weather.utils.RedisUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @PostConstruct和@PreDestroy注解 被@PostConstruct修饰的方法会在服务器加载Servlet的时候运行，并且只会被服务器调用一次，类似于Serclet的inti()方法。
 * 被@PostConstruct修饰的方法会在构造函数之后，init()方法之前运行。
 * <p>
 * 被@PreConstruct修饰的方法会在服务器卸载Servlet的时候运行，并且只会被服务器调用一次，类似于Servlet的destroy()方法。
 * 被@PreConstruct修饰的方法会在destroy()方法之后运行，在Servlet被彻底卸载之前。
 */
@Log4j2
@Component
public class StartLoadDataToRedis {

    @Autowired
    RedisUtil redisUtil;

    @Autowired
    CityDao cityDao;

    @PostConstruct
    public void startLoadDataToRedis() {
        List<CityDO> cityDOList = cityDao.findAll();
        log.info("启动加载数据" + cityDOList.size() + "条到redis");
        Map<String, String> map = new HashMap<>();
        for (CityDO cityDO : cityDOList) {
            map.put(cityDO.getCityZh(), JSONObject.toJSONString(cityDO));
        }
        // 对象与字符串互转
//            aa = JSONObject.toJSONString(cityDO);
//        CityDO cityDO = JSONObject.parseObject(aa, CityDO.class);
        redisUtil.putBoundHashOps("citys", map);
        log.info("往redis存放" + map.size() + "条到数据成功");
    }

    @PreDestroy
    public void endDelDataFromRedis() {
        log.info("容器销毁");
    }
}
