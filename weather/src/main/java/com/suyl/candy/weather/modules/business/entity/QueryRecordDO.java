package com.suyl.candy.weather.modules.business.entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@ToString
@Entity
@Table(name = "buss_query_record")
public class QueryRecordDO implements Serializable {

    /**
     * 用户id，主键自增
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 创建时间
     */
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    /**
     * 查询状态
     */
    @Column(length = 5)
    private String status;

    /**
     * 查询信息
     */
    @Column
    private String message;

    /**
     * 查询时间
     */
    @Column(length = 32)
    private String time;

    /**
     * 查询日期
     */
    @Column(length = 8)
    private String date;

    /**
     * 更新时间
     */
    @Column(length = 8)
    private String updateTime;

    /**
     * 城市
     */
    @Column(length = 32)
    private String city;

    /**
     * 城市code
     */
    @Column(length = 16)
    private String cityKey;

    /**
     * 省份
     */
    @Column(length = 32)
    private String parent;

    /**
     * 备注
     */
    @Column
    private String remarks;
}
