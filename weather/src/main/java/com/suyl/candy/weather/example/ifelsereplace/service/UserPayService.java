package com.suyl.candy.weather.example.ifelsereplace.service;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public interface UserPayService {

    public BigDecimal quote(BigDecimal orderPrice);
}
