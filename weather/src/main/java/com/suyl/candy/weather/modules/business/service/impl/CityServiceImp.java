package com.suyl.candy.weather.modules.business.service.impl;

import com.suyl.candy.weather.modules.business.dao.CityDao;
import com.suyl.candy.weather.modules.business.entity.CityDO;
import com.suyl.candy.weather.modules.business.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CityServiceImp implements CityService {

    @Autowired
    CityDao cityDao;

    @Override
    public CityDO findByName(String name) {
        return cityDao.findByCityEn(name);
    }

    @Override
    public List<Long> findAllByCityZhEQLeaderZh() {
        return cityDao.findAllByCityZhEQLeaderZh();
    }

    @Override
    public List<Long> findAllId() {
        return cityDao.findAllId();
    }
}
